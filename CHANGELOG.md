
## 0.6.6 [12-02-2024]

* Changes made at 2024.12.02_14:30PM

See merge request itentialopensource/adapters/adapter-128technology!28

---

## 0.6.5 [10-15-2024]

* Changes made at 2024.10.14_21:00PM

See merge request itentialopensource/adapters/adapter-128technology!24

---

## 0.6.4 [08-21-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-128technology!22

---

## 0.6.3 [08-14-2024]

* Changes made at 2024.08.14_19:22PM

See merge request itentialopensource/adapters/adapter-128technology!21

---

## 0.6.2 [08-07-2024]

* Changes made at 2024.08.06_21:03PM

See merge request itentialopensource/adapters/adapter-128technology!20

---

## 0.6.1 [08-05-2024]

* Changes made at 2024.08.05_14:45PM

See merge request itentialopensource/adapters/adapter-128technology!19

---

## 0.6.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-128technology!18

---

## 0.5.7 [03-28-2024]

* Changes made at 2024.03.28_13:24PM

See merge request itentialopensource/adapters/cloud/adapter-128technology!17

---

## 0.5.6 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-128technology!16

---

## 0.5.5 [03-13-2024]

* Changes made at 2024.03.13_12:48PM

See merge request itentialopensource/adapters/cloud/adapter-128technology!15

---

## 0.5.4 [03-11-2024]

* Changes made at 2024.03.11_15:36PM

See merge request itentialopensource/adapters/cloud/adapter-128technology!14

---

## 0.5.3 [02-28-2024]

* Changes made at 2024.02.28_11:52AM

See merge request itentialopensource/adapters/cloud/adapter-128technology!13

---

## 0.5.2 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-128technology!12

---

## 0.5.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-128technology!10

---

## 0.5.0 [11-13-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-128technology!9

---

## 0.4.2 [06-23-2022]

* Update to CALLS.md to test header

See merge request itentialopensource/adapters/cloud/adapter-128technology!8

---

## 0.4.1 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/cloud/adapter-128technology!7

---

## 0.4.0 [05-18-2022] & 0.3.0 [01-19-2022]

- Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
- Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
- Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
- Add scripts for easier authentication, removing hooks, etc
- Script changes (install script as well as database changes in other scripts)
- Double # of path vars on generic call
- Update versions of foundation (e.g. adapter-utils)
- Update npm publish so it supports https
- Update dependencies
- Add preinstall for minimist
- Fix new lint issues that came from eslint dependency change
- Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
- Add the adapter type in the package.json
- Add AdapterInfo.js script
- Add json-query dependency
- Add the propertiesDecorators.json for product encryption
- Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
- Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
- Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
- Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
- Add AdapterInfo.json
- Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-128technology!5

---

## 0.2.4 [02-25-2021]

- migration to bring up to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-128technology!4

---

## 0.2.3 [07-06-2020] & 0.2.2 [07-01-2020]

- Migration of the adapter to the latest adapter foundation - specific details are in ADAPT-168

See merge request itentialopensource/adapters/cloud/adapter-128technology!3

---

## 0.2.1 [06-02-2020]

- Only change the sampleProperties so no code changes

See merge request itentialopensource/adapters/cloud/adapter-128technology!2

---

## 0.2.0 [04-07-2020]

- Fix the method names that were generated automatically as they are bad as a result of not having operation ids.

See merge request itentialopensource/adapters/cloud/adapter-128technology!1

---
