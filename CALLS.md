## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for 128 Technology. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for 128 Technology.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for 128technology. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAlarm(router, node, shelvedStatus = 'SHELVED', callback)</td>
    <td style="padding:15px">Gets all the current alarms for each router and node</td>
    <td style="padding:15px">{base_path}/{version}/alarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterAlarm(router, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the current alarms for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/alarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeAlarm(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the current alarms for the supplied router and node.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/alarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAnalyticsRunningTransform(body, callback)</td>
    <td style="padding:15px">Gets analytics data transformed at intervals over a time window.</td>
    <td style="padding:15px">{base_path}/{version}/analytics/runningTransform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAnalyticsTransform(body, callback)</td>
    <td style="padding:15px">Gets transformed analytics data.</td>
    <td style="padding:15px">{base_path}/{version}/analytics/transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetIdControl(assetId, body, callback)</td>
    <td style="padding:15px">Sends a command to a registered Asset.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAudit(router, start, end, callback)</td>
    <td style="padding:15px">Gets the historical audit logs.</td>
    <td style="padding:15px">{base_path}/{version}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditDownload(router, start, end, format = 'json', group, callback)</td>
    <td style="padding:15px">Downloads the historical audit logs.</td>
    <td style="padding:15px">{base_path}/{version}/audit/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigCommit(body, callback)</td>
    <td style="padding:15px">Commit the candidate configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigExport(body, callback)</td>
    <td style="padding:15px">Exports a configuration to the local filesystem.</td>
    <td style="padding:15px">{base_path}/{version}/config/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigExport(callback)</td>
    <td style="padding:15px">Retrieve a list of exported configurations.</td>
    <td style="padding:15px">{base_path}/{version}/config/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigGetJson(source = 'candidate', callback)</td>
    <td style="padding:15px">Gets the entire configuration for 128T.</td>
    <td style="padding:15px">{base_path}/{version}/config/getJSON?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigImport(body, callback)</td>
    <td style="padding:15px">Import a configuration located on the local filesystem.</td>
    <td style="padding:15px">{base_path}/{version}/config/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRevertToRunning(callback)</td>
    <td style="padding:15px">Reverts the candidate config</td>
    <td style="padding:15px">{base_path}/{version}/config/revertToRunning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigValidate(body, callback)</td>
    <td style="padding:15px">Validates the candidate configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigVersion(datastore = 'candidate', callback)</td>
    <td style="padding:15px">Gets the version of the configuration.</td>
    <td style="padding:15px">{base_path}/{version}/config/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStore(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the 128T configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStore(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Modifies a 128T in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthority(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the authority configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthority(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Modifies a authority in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityDistrict(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the district configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityDistrict(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a district resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityDistrictClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing district in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityDistrict2(configStore = 'running', district, callback)</td>
    <td style="padding:15px">Gets the district configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityDistrict(configStore = 'running', district, body, callback)</td>
    <td style="padding:15px">Modifies a district in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityDistrict(configStore = 'running', district, callback)</td>
    <td style="padding:15px">Deletes a district in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/district/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityDscpMap(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the dscp-map configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityDscpMap(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a dscp-map resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityDscpMapClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing dscp-map in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityDscpMapDscpmap(configStore = 'running', dscpmap, callback)</td>
    <td style="padding:15px">Gets the dscp-map configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityDscpMapDscpmap(configStore = 'running', dscpmap, body, callback)</td>
    <td style="padding:15px">Modifies a dscp-map in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityDscpMapDscpmap(configStore = 'running', dscpmap, callback)</td>
    <td style="padding:15px">Deletes a dscp-map in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDscpMapDscpmapPrioritization(configStore = 'running', dscpmap, callback)</td>
    <td style="padding:15px">Gets the dscp-prioritization configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDscpMapDscpmapPrioritization(configStore = 'running', dscpmap, body, callback)</td>
    <td style="padding:15px">Creates a dscp-prioritization resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMapDscpmapDscpPrioritizationClone(configStore = 'running', dscpmap, body, callback)</td>
    <td style="padding:15px">Clone an existing dscp-prioritization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMapDscpmapDscpPrioritizationDscpprioritization(configStore = 'running', dscpmap, dscpprioritization, callback)</td>
    <td style="padding:15px">Gets the dscp-prioritization configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMapDscpmapDscpPrioritizationDscpprioritization(configStore = 'running', dscpmap, dscpprioritization, body, callback)</td>
    <td style="padding:15px">Modifies a dscp-prioritization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMapDscpmapDscpPrioritizationDscpprioritization(configStore = 'running', dscpmap, dscpprioritization, callback)</td>
    <td style="padding:15px">Deletes a dscp-prioritization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDscpPrioritizationDscpprioritizationRange(configStore = 'running', dscpmap, dscpprioritization, callback)</td>
    <td style="padding:15px">Gets the dscp-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDscpPrioritizationDscpprioritizationRange(configStore = 'running', dscpmap, dscpprioritization, body, callback)</td>
    <td style="padding:15px">Creates a dscp-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPrioritizationDscpprioritizationDscpRangeClone(configStore = 'running', dscpmap, dscpprioritization, body, callback)</td>
    <td style="padding:15px">Clone an existing dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPrioritizationDscpprioritizationDscpRangeDscprange(configStore = 'running', dscpmap, dscpprioritization, dscprange, callback)</td>
    <td style="padding:15px">Gets the dscp-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPrioritizationDscpprioritizationDscpRangeDscprange(configStore = 'running', dscpmap, dscpprioritization, dscprange, body, callback)</td>
    <td style="padding:15px">Modifies a dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPrioritizationDscpprioritizationDscpRangeDscprange(configStore = 'running', dscpmap, dscpprioritization, dscprange, callback)</td>
    <td style="padding:15px">Deletes a dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-prioritization/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMapDscpmapDscpTrafficClass(configStore = 'running', dscpmap, callback)</td>
    <td style="padding:15px">Gets the dscp-traffic-class configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMapDscpmapDscpTrafficClass(configStore = 'running', dscpmap, body, callback)</td>
    <td style="padding:15px">Creates a dscp-traffic-class resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDscpmapDscpTrafficClassClone(configStore = 'running', dscpmap, body, callback)</td>
    <td style="padding:15px">Clone an existing dscp-traffic-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDscpmapDscpTrafficClassDscptrafficclass(configStore = 'running', dscpmap, dscptrafficclass, callback)</td>
    <td style="padding:15px">Gets the dscp-traffic-class configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigDscpmapDscpTrafficClassDscptrafficclass(configStore = 'running', dscpmap, dscptrafficclass, body, callback)</td>
    <td style="padding:15px">Modifies a dscp-traffic-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigDscpmapDscpTrafficClassDscptrafficclass(configStore = 'running', dscpmap, dscptrafficclass, callback)</td>
    <td style="padding:15px">Deletes a dscp-traffic-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTrafficClassDscptrafficclassDscpRange(configStore = 'running', dscpmap, dscptrafficclass, callback)</td>
    <td style="padding:15px">Gets the dscp-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTrafficClassDscptrafficclassDscpRange(configStore = 'running', dscpmap, dscptrafficclass, body, callback)</td>
    <td style="padding:15px">Creates a dscp-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigClassDscptrafficclassDscpRangeClone(configStore = 'running', dscpmap, dscptrafficclass, body, callback)</td>
    <td style="padding:15px">Clone an existing dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigClassDscptrafficclassDscpRangeDscprange(configStore = 'running', dscpmap, dscptrafficclass, dscprange, callback)</td>
    <td style="padding:15px">Gets the dscp-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigClassDscptrafficclassDscpRangeDscprange(configStore = 'running', dscpmap, dscptrafficclass, dscprange, body, callback)</td>
    <td style="padding:15px">Modifies a dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigClassDscptrafficclassDscpRangeDscprange(configStore = 'running', dscpmap, dscptrafficclass, dscprange, callback)</td>
    <td style="padding:15px">Deletes a dscp-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/dscp-map/{pathv2}/dscp-traffic-class/{pathv3}/dscp-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityIpfixCollector(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the ipfix-collector configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityIpfixCollector(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a ipfix-collector resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityIpfixCollectorClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing ipfix-collector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityIpfixCollectorIpfixcollector(configStore = 'running', ipfixcollector, callback)</td>
    <td style="padding:15px">Gets the ipfix-collector configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityIpfixCollectorIpfixcollector(configStore = 'running', ipfixcollector, body, callback)</td>
    <td style="padding:15px">Modifies a ipfix-collector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityIpfixCollectorIpfixcollector(configStore = 'running', ipfixcollector, callback)</td>
    <td style="padding:15px">Deletes a ipfix-collector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ipfix-collector/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityLdapServer(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the ldap-server configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityLdapServer(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a ldap-server resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityLdapServerClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing ldap-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityLdapServerLdapserver(configStore = 'running', ldapserver, callback)</td>
    <td style="padding:15px">Gets the ldap-server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityLdapServerLdapserver(configStore = 'running', ldapserver, body, callback)</td>
    <td style="padding:15px">Modifies a ldap-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityLdapServerLdapserver(configStore = 'running', ldapserver, callback)</td>
    <td style="padding:15px">Deletes a ldap-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/ldap-server/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityManagementServiceGeneration(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the management-service-generation configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/management-service-generation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityManagementServiceGeneration(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Modifies a management-service-generation in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/management-service-generation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRemoteLogin(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the remote-login configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/remote-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityRemoteLogin(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Modifies a remote-login in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/remote-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRouter(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the router configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityRouter(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a router resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityRouterClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing router in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRouter2(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the router configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityRouter(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a router in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityRouter(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Deletes a router in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterApplicationIdentification(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the application-identification configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/application-identification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRouterApplicationIdentification(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a application-identification in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/application-identification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterBfd(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the bfd configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRouterBfd(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a bfd in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterDistrictSettings(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the district-settings configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterDistrictSettings(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a district-settings resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterDistrictSettingsClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing district-settings in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterDistrictSettingsDistrictsettings(configStore = 'running', router, districtsettings, callback)</td>
    <td style="padding:15px">Gets the district-settings configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterDistrictSettingsDistrictsettings(configStore = 'running', router, districtsettings, body, callback)</td>
    <td style="padding:15px">Modifies a district-settings in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterDistrictSettingsDistrictsettings(configStore = 'running', router, districtsettings, callback)</td>
    <td style="padding:15px">Deletes a district-settings in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPeerPathSlaMetricsAdvertisement(configStore = 'running', router, districtsettings, callback)</td>
    <td style="padding:15px">Gets the step-peer-path-sla-metrics-advertisement configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/{pathv3}/step-peer-path-sla-metrics-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPeerPathSlaMetricsAdvertisement(configStore = 'running', router, districtsettings, body, callback)</td>
    <td style="padding:15px">Modifies a step-peer-path-sla-metrics-advertisement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/district-settings/{pathv3}/step-peer-path-sla-metrics-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterDns(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the dns-config configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterDns(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a dns-config resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterDnsClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing dns-config in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterDnsDnsconfig(configStore = 'running', router, dnsconfig, callback)</td>
    <td style="padding:15px">Gets the dns-config configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterDnsDnsconfig(configStore = 'running', router, dnsconfig, body, callback)</td>
    <td style="padding:15px">Modifies a dns-config in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterDnsDnsconfig(configStore = 'running', router, dnsconfig, callback)</td>
    <td style="padding:15px">Deletes a dns-config in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/dns-config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterEntitlement(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the entitlement configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/entitlement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRouterEntitlement(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a entitlement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/entitlement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterManagementServiceGeneration(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the management-service-generation configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/management-service-generation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterManagementServiceGeneration(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a management-service-generation in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/management-service-generation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterNatPool(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the nat-pool configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterNatPool(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a nat-pool resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterNatPoolClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing nat-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterNatPoolNatpool(configStore = 'running', router, natpool, callback)</td>
    <td style="padding:15px">Gets the nat-pool configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterNatPoolNatpool(configStore = 'running', router, natpool, body, callback)</td>
    <td style="padding:15px">Modifies a nat-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterNatPoolNatpool(configStore = 'running', router, natpool, callback)</td>
    <td style="padding:15px">Deletes a nat-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNatPoolNatpoolAddress(configStore = 'running', router, natpool, callback)</td>
    <td style="padding:15px">Gets the address-pool configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNatPoolNatpoolAddress(configStore = 'running', router, natpool, body, callback)</td>
    <td style="padding:15px">Creates a address-pool resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPoolNatpoolAddressClone(configStore = 'running', router, natpool, body, callback)</td>
    <td style="padding:15px">Clone an existing address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPoolNatpoolAddressMove(configStore = 'running', router, natpool, body, callback)</td>
    <td style="padding:15px">Move a address-pool to a new position in the list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPoolNatpoolAddressAddresspool(configStore = 'running', router, natpool, addresspool, callback)</td>
    <td style="padding:15px">Gets the address-pool configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPoolNatpoolAddressAddresspool(configStore = 'running', router, natpool, addresspool, body, callback)</td>
    <td style="padding:15px">Modifies a address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPoolNatpoolAddressAddresspool(configStore = 'running', router, natpool, addresspool, callback)</td>
    <td style="padding:15px">Deletes a address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/nat-pool/{pathv3}/address-pool/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterNode(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the node configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterNode(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a node resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterNodeClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing node in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterNode(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the node configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterNode(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a node in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterNode(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Deletes a node in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterNodeDeviceInterface(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the device-interface configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterNodeDeviceInterface(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Creates a device-interface resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNodeDeviceInterfaceClone(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Clone an existing device-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNodeDeviceInterfaceDeviceinterface(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the device-interface configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNodeDeviceInterfaceDeviceinterface(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a device-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNodeDeviceInterfaceDeviceinterface(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Deletes a device-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceInterfaceDeviceinterfaceLoadBalancing(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the load-balancing configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/load-balancing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigDeviceInterfaceDeviceinterfaceLoadBalancing(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a load-balancing in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/load-balancing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNodeDeviceInterfaceDeviceinterfaceLte(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the lte configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/lte?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNodeDeviceInterfaceDeviceinterfaceLte(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a lte in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/lte?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceInterfaceDeviceinterfaceLteAuthentication(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the authentication configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/lte/authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigDeviceInterfaceDeviceinterfaceLteAuthentication(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a authentication in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/lte/authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceInterfaceDeviceinterfaceNetwork(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the network-interface configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDeviceInterfaceDeviceinterfaceNetwork(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Creates a network-interface resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigInterfaceDeviceinterfaceNetworkClone(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing network-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigInterfaceDeviceinterfaceNetworkNetworkinterface(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the network-interface configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigInterfaceDeviceinterfaceNetworkNetworkinterface(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Modifies a network-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigInterfaceDeviceinterfaceNetworkNetworkinterface(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Deletes a network-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceAddress(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the address configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceAddress(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a address resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceAddressClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceAddress(configStore = 'running', router, node, deviceinterface, networkinterface, address, callback)</td>
    <td style="padding:15px">Gets the address configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkInterfaceNetworkinterfaceAddress(configStore = 'running', router, node, deviceinterface, networkinterface, address, body, callback)</td>
    <td style="padding:15px">Modifies a address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNetworkInterfaceNetworkinterfaceAddress(configStore = 'running', router, node, deviceinterface, networkinterface, address, callback)</td>
    <td style="padding:15px">Deletes a address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceAddressHostService(configStore = 'running', router, node, deviceinterface, networkinterface, address, callback)</td>
    <td style="padding:15px">Gets the host-service configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkinterfaceAddressHostService(configStore = 'running', router, node, deviceinterface, networkinterface, address, body, callback)</td>
    <td style="padding:15px">Creates a host-service resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressHostServiceClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, body, callback)</td>
    <td style="padding:15px">Clone an existing host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Gets the host-service configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Modifies a host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAddressHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Deletes a host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceAccessPolicy(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceAccessPolicy(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a access-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceHostserviceAccessPolicyClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceHostserviceAccessPolicyAccesspolicy(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, accesspolicy, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceHostserviceAccessPolicyAccesspolicy(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, accesspolicy, body, callback)</td>
    <td style="padding:15px">Modifies a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceHostserviceAccessPolicyAccesspolicy(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, accesspolicy, callback)</td>
    <td style="padding:15px">Deletes a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/access-policy/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceAddressPool(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Gets the address-pool configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceAddressPool(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a address-pool resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceHostserviceAddressPoolClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceHostserviceAddressPoolAddresspool(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the address-pool configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceHostserviceAddressPoolAddresspool(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Modifies a address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceHostserviceAddressPoolAddresspool(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Deletes a address-pool in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostserviceAddressPoolAddresspoolCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the custom configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostserviceAddressPoolAddresspoolCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Creates a custom resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressPoolAddresspoolCustomClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Clone an existing custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressPoolAddresspoolCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, custom, callback)</td>
    <td style="padding:15px">Gets the custom configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressPoolAddresspoolCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, custom, body, callback)</td>
    <td style="padding:15px">Modifies a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAddressPoolAddresspoolCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, custom, callback)</td>
    <td style="padding:15px">Deletes a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressPoolAddresspoolStaticAssignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the static-assignment configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressPoolAddresspoolStaticAssignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Creates a static-assignment resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPoolAddresspoolStaticAssignmentClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Clone an existing static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPoolAddresspoolStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Gets the static-assignment configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPoolAddresspoolStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Modifies a static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPoolAddresspoolStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Deletes a static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddresspoolStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Gets the custom configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddresspoolStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a custom resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticAssignmentStaticassignmentCustomClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, custom, callback)</td>
    <td style="padding:15px">Gets the custom configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, custom, body, callback)</td>
    <td style="padding:15px">Modifies a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, custom, callback)</td>
    <td style="padding:15px">Deletes a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/custom/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticAssignmentStaticassignmentRoute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Gets the static-route configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticAssignmentStaticassignmentRoute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a static-route resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAssignmentStaticassignmentStaticRouteClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAssignmentStaticassignmentStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, staticroute, callback)</td>
    <td style="padding:15px">Gets the static-route configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAssignmentStaticassignmentStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, staticroute, body, callback)</td>
    <td style="padding:15px">Modifies a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAssignmentStaticassignmentStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, staticroute, callback)</td>
    <td style="padding:15px">Deletes a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/static-route/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigVendorIdentifyingSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigVendorIdentifyingSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a vendor-identifying-vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigIdentifyingVendorSpecificInformationClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendoridentifyingvendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-identifying-vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAssignmentStaticassignmentVendorSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAssignmentStaticassignmentVendorSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticassignmentVendorSpecificInformationClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticassignmentVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStaticassignmentVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStaticassignmentVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticassignment, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-assignment/{pathv9}/vendor-specific-information/{pathv10}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressPoolAddresspoolStaticRoute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the static-route configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressPoolAddresspoolStaticRoute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Creates a static-route resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPoolAddresspoolStaticRouteClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Clone an existing static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPoolAddresspoolStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticroute, callback)</td>
    <td style="padding:15px">Gets the static-route configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPoolAddresspoolStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticroute, body, callback)</td>
    <td style="padding:15px">Modifies a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPoolAddresspoolStaticRouteStaticroute(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, staticroute, callback)</td>
    <td style="padding:15px">Deletes a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigVendorIdentifyingSpecificInformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigVendorIdentifyingSpecificInformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Creates a vendor-identifying-vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigIdentifyingVendorSpecificInformationClone2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendoridentifyingvendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPoolAddresspoolVendorSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPoolAddresspoolVendorSpecificInformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Creates a vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddresspoolVendorSpecificInformationClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddresspoolVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddresspoolVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAddresspoolVendorSpecificInformationVendorspecificinformation(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, addresspool, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/address-pool/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceStaticAssignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Gets the static-assignment configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceStaticAssignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a static-assignment resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceHostserviceStaticAssignmentClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceHostserviceStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Gets the static-assignment configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceHostserviceStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Modifies a static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceHostserviceStaticAssignmentStaticassignment(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Deletes a static-assignment in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostserviceStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Gets the custom configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostserviceStaticAssignmentStaticassignmentCustom(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a custom resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticAssignmentStaticassignmentCustomClone2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticAssignmentStaticassignmentCustom2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, custom, callback)</td>
    <td style="padding:15px">Gets the custom configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStaticAssignmentStaticassignmentCustom2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, custom, body, callback)</td>
    <td style="padding:15px">Modifies a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStaticAssignmentStaticassignmentCustom2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, custom, callback)</td>
    <td style="padding:15px">Deletes a custom in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/custom/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticAssignmentStaticassignmentRoute2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Gets the static-route configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticAssignmentStaticassignmentRoute2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a static-route resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAssignmentStaticassignmentStaticRouteClone2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAssignmentStaticassignmentStaticRouteStaticroute2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, staticroute, callback)</td>
    <td style="padding:15px">Gets the static-route configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAssignmentStaticassignmentStaticRouteStaticroute2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, staticroute, body, callback)</td>
    <td style="padding:15px">Modifies a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAssignmentStaticassignmentStaticRouteStaticroute2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, staticroute, callback)</td>
    <td style="padding:15px">Deletes a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/static-route/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigVendorIdentifyingSpecificInformation3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigVendorIdentifyingSpecificInformation3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a vendor-identifying-vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigIdentifyingVendorSpecificInformationClone3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-identifying-vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendoridentifyingvendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigIdentifyingVendorSpecificInformationVendoridentifyingvendorspecificinformation3(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendoridentifyingvendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-identifying-vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-identifying-vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAssignmentStaticassignmentVendorSpecificInformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAssignmentStaticassignmentVendorSpecificInformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Creates a vendor-specific-information resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticassignmentVendorSpecificInformationClone2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, body, callback)</td>
    <td style="padding:15px">Clone an existing vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticassignmentVendorSpecificInformationVendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Gets the vendor-specific-information configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStaticassignmentVendorSpecificInformationVendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendorspecificinformation, body, callback)</td>
    <td style="padding:15px">Modifies a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStaticassignmentVendorSpecificInformationVendorspecificinformation2(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, staticassignment, vendorspecificinformation, callback)</td>
    <td style="padding:15px">Deletes a vendor-specific-information in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/static-assignment/{pathv8}/vendor-specific-information/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, callback)</td>
    <td style="padding:15px">Gets the transport configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a transport resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceTransportClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, callback)</td>
    <td style="padding:15px">Gets the transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Modifies a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, callback)</td>
    <td style="padding:15px">Deletes a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostserviceTransportPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostserviceTransportPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTransportPortRangeClone(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTransportPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTransportPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigTransportPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, address, hostservice, transport, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/address/{pathv6}/host-service/{pathv7}/transport/{pathv8}/port-range/{pathv9}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceAdjacency(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the adjacency configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceAdjacency(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a adjacency resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceAdjacencyClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing adjacency in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceAdjacency(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the adjacency configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkInterfaceNetworkinterfaceAdjacency(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a adjacency in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNetworkInterfaceNetworkinterfaceAdjacency(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Deletes a adjacency in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceAdjacencyBfd(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the bfd configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceAdjacencyBfd(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a bfd in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdjacencyNatKeepAlive(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the nat-keep-alive configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/nat-keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdjacencyNatKeepAlive(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a nat-keep-alive in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/nat-keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdjacencyPathMtuDiscovery(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the path-mtu-discovery configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdjacencyPathMtuDiscovery(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a path-mtu-discovery in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceAdjacencyPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkinterfaceAdjacencyPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAdjacencyPortRangeClone(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdjacencyPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdjacencyPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAdjacencyPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdjacencyPostEncryptionPadding(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the post-encryption-padding configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/post-encryption-padding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdjacencyPostEncryptionPadding(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a post-encryption-padding in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/post-encryption-padding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceAdjacencySessionOptimization(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the session-optimization configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceAdjacencySessionOptimization(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a session-optimization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdjacencyStepPeerPathAdvertisement(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the step-peer-path-advertisement configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdjacencyStepPeerPathAdvertisement(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a step-peer-path-advertisement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPeerPathAdvertisementSlaMetrics(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the sla-metrics configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPeerPathAdvertisementSlaMetrics(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a sla-metrics in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSlaMetricsDecreaseReportDelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the decrease-report-delay configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSlaMetricsDecreaseReportDelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Creates a decrease-report-delay resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMetricsDecreaseReportDelayClone(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Clone an existing decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMetricsDecreaseReportDelayDecreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, decreasereportdelay, callback)</td>
    <td style="padding:15px">Gets the decrease-report-delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMetricsDecreaseReportDelayDecreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, decreasereportdelay, body, callback)</td>
    <td style="padding:15px">Modifies a decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMetricsDecreaseReportDelayDecreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, decreasereportdelay, callback)</td>
    <td style="padding:15px">Deletes a decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSlaMetricsIncreaseReportDelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the increase-report-delay configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSlaMetricsIncreaseReportDelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Creates a increase-report-delay resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMetricsIncreaseReportDelayClone(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Clone an existing increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMetricsIncreaseReportDelayIncreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, increasereportdelay, callback)</td>
    <td style="padding:15px">Gets the increase-report-delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMetricsIncreaseReportDelayIncreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, increasereportdelay, body, callback)</td>
    <td style="padding:15px">Modifies a increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMetricsIncreaseReportDelayIncreasereportdelay(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, increasereportdelay, callback)</td>
    <td style="padding:15px">Deletes a increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdvertisementSlaMetricsSignificanceThreshold(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the significance-threshold configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/significance-threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdvertisementSlaMetricsSignificanceThreshold(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a significance-threshold in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/step-peer-path-advertisement/sla-metrics/significance-threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceAdjacencyUdpTransform(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, callback)</td>
    <td style="padding:15px">Gets the udp-transform configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceAdjacencyUdpTransform(configStore = 'running', router, node, deviceinterface, networkinterface, adjacency, body, callback)</td>
    <td style="padding:15px">Modifies a udp-transform in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/adjacency/{pathv6}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceHostService(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the host-service configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceHostService(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a host-service resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigInterfaceNetworkinterfaceHostServiceClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigInterfaceNetworkinterfaceHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, callback)</td>
    <td style="padding:15px">Gets the host-service configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigInterfaceNetworkinterfaceHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, body, callback)</td>
    <td style="padding:15px">Modifies a host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigInterfaceNetworkinterfaceHostServiceHostservice(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, callback)</td>
    <td style="padding:15px">Deletes a host-service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceAccessPolicy2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceAccessPolicy2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a access-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceHostserviceAccessPolicyClone2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceHostserviceAccessPolicyAccesspolicy2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, accesspolicy, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceHostserviceAccessPolicyAccesspolicy2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, accesspolicy, body, callback)</td>
    <td style="padding:15px">Modifies a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceHostserviceAccessPolicyAccesspolicy2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, accesspolicy, callback)</td>
    <td style="padding:15px">Deletes a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/access-policy/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, callback)</td>
    <td style="padding:15px">Gets the transport configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkinterfaceHostServiceHostserviceTransport(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, body, callback)</td>
    <td style="padding:15px">Creates a transport resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostServiceHostserviceTransportClone2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, body, callback)</td>
    <td style="padding:15px">Clone an existing transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostServiceHostserviceTransport2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, callback)</td>
    <td style="padding:15px">Gets the transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigHostServiceHostserviceTransport2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Modifies a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigHostServiceHostserviceTransport2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, callback)</td>
    <td style="padding:15px">Deletes a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigHostserviceTransportPortRange2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigHostserviceTransportPortRange2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTransportPortRangeClone2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTransportPortRangePortrange2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTransportPortRangePortrange2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigTransportPortRangePortrange2(configStore = 'running', router, node, deviceinterface, networkinterface, hostservice, transport, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/host-service/{pathv6}/transport/{pathv7}/port-range/{pathv8}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceIfcfgOption(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the ifcfg-option configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceIfcfgOption(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a ifcfg-option resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigInterfaceNetworkinterfaceIfcfgOptionClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing ifcfg-option in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigInterfaceNetworkinterfaceIfcfgOptionIfcfgoption(configStore = 'running', router, node, deviceinterface, networkinterface, ifcfgoption, callback)</td>
    <td style="padding:15px">Gets the ifcfg-option configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigInterfaceNetworkinterfaceIfcfgOptionIfcfgoption(configStore = 'running', router, node, deviceinterface, networkinterface, ifcfgoption, body, callback)</td>
    <td style="padding:15px">Modifies a ifcfg-option in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigInterfaceNetworkinterfaceIfcfgOptionIfcfgoption(configStore = 'running', router, node, deviceinterface, networkinterface, ifcfgoption, callback)</td>
    <td style="padding:15px">Deletes a ifcfg-option in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/ifcfg-option/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceManagementVector(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the management-vector configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/management-vector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkInterfaceNetworkinterfaceManagementVector(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Modifies a management-vector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/management-vector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceNeighbor(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the neighbor configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceNeighbor(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a neighbor resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceNeighborClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceNeighbor(configStore = 'running', router, node, deviceinterface, networkinterface, neighbor, callback)</td>
    <td style="padding:15px">Gets the neighbor configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkInterfaceNetworkinterfaceNeighbor(configStore = 'running', router, node, deviceinterface, networkinterface, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNetworkInterfaceNetworkinterfaceNeighbor(configStore = 'running', router, node, deviceinterface, networkinterface, neighbor, callback)</td>
    <td style="padding:15px">Deletes a neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighbor/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceNeighborhood(configStore = 'running', router, node, deviceinterface, networkinterface, callback)</td>
    <td style="padding:15px">Gets the neighborhood configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDeviceinterfaceNetworkInterfaceNetworkinterfaceNeighborhood(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Creates a neighborhood resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceNeighborhoodClone(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Clone an existing neighborhood in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkInterfaceNetworkinterfaceNeighborhoodMove(configStore = 'running', router, node, deviceinterface, networkinterface, body, callback)</td>
    <td style="padding:15px">Move a neighborhood to a new position in the list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkInterfaceNetworkinterfaceNeighborhood(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the neighborhood configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkInterfaceNetworkinterfaceNeighborhood(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a neighborhood in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNetworkInterfaceNetworkinterfaceNeighborhood(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Deletes a neighborhood in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceNeighborhoodBfd(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the bfd configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceNeighborhoodBfd(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a bfd in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborhoodNatKeepAlive(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the nat-keep-alive configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/nat-keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborhoodNatKeepAlive(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a nat-keep-alive in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/nat-keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborhoodPathMtuDiscovery(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the path-mtu-discovery configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborhoodPathMtuDiscovery(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a path-mtu-discovery in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceNeighborhoodPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNetworkinterfaceNeighborhoodPortRange(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNeighborhoodPortRangeClone(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborhoodPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborhoodPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNeighborhoodPortRangePortrange(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/port-range/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborhoodPostEncryptionPadding(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the post-encryption-padding configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/post-encryption-padding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborhoodPostEncryptionPadding(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a post-encryption-padding in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/post-encryption-padding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceNeighborhoodSessionOptimization(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the session-optimization configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceNeighborhoodSessionOptimization(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a session-optimization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborhoodStepPeerPathAdvertisement(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the step-peer-path-advertisement configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborhoodStepPeerPathAdvertisement(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a step-peer-path-advertisement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPeerPathAdvertisementSlaMetrics2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the sla-metrics configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPeerPathAdvertisementSlaMetrics2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a sla-metrics in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSlaMetricsDecreaseReportDelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the decrease-report-delay configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSlaMetricsDecreaseReportDelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Creates a decrease-report-delay resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMetricsDecreaseReportDelayClone2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Clone an existing decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMetricsDecreaseReportDelayDecreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, decreasereportdelay, callback)</td>
    <td style="padding:15px">Gets the decrease-report-delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMetricsDecreaseReportDelayDecreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, decreasereportdelay, body, callback)</td>
    <td style="padding:15px">Modifies a decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMetricsDecreaseReportDelayDecreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, decreasereportdelay, callback)</td>
    <td style="padding:15px">Deletes a decrease-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/decrease-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSlaMetricsIncreaseReportDelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the increase-report-delay configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSlaMetricsIncreaseReportDelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Creates a increase-report-delay resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigMetricsIncreaseReportDelayClone2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Clone an existing increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMetricsIncreaseReportDelayIncreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, increasereportdelay, callback)</td>
    <td style="padding:15px">Gets the increase-report-delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMetricsIncreaseReportDelayIncreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, increasereportdelay, body, callback)</td>
    <td style="padding:15px">Modifies a increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMetricsIncreaseReportDelayIncreasereportdelay2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, increasereportdelay, callback)</td>
    <td style="padding:15px">Deletes a increase-report-delay in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/increase-report-delay/{pathv7}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAdvertisementSlaMetricsSignificanceThreshold2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the significance-threshold configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/significance-threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAdvertisementSlaMetricsSignificanceThreshold2(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a significance-threshold in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/step-peer-path-advertisement/sla-metrics/significance-threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNetworkinterfaceNeighborhoodUdpTransform(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, callback)</td>
    <td style="padding:15px">Gets the udp-transform configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNetworkinterfaceNeighborhoodUdpTransform(configStore = 'running', router, node, deviceinterface, networkinterface, neighborhood, body, callback)</td>
    <td style="padding:15px">Modifies a udp-transform in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/network-interface/{pathv5}/neighborhood/{pathv6}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNodeDeviceInterfaceDeviceinterfacePppoe(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the pppoe configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/pppoe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNodeDeviceInterfaceDeviceinterfacePppoe(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a pppoe in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/pppoe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceInterfaceDeviceinterfaceSessionOptimization(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the session-optimization configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigDeviceInterfaceDeviceinterfaceSessionOptimization(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a session-optimization in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/session-optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDeviceInterfaceDeviceinterfaceTrafficEngineering(configStore = 'running', router, node, deviceinterface, callback)</td>
    <td style="padding:15px">Gets the traffic-engineering configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/traffic-engineering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigDeviceInterfaceDeviceinterfaceTrafficEngineering(configStore = 'running', router, node, deviceinterface, body, callback)</td>
    <td style="padding:15px">Modifies a traffic-engineering in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/device-interface/{pathv4}/traffic-engineering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterNodeReachabilityDetection(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the reachability-detection configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/reachability-detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterNodeReachabilityDetection(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a reachability-detection in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/reachability-detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterNodeSshKeepalive(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the ssh-keepalive configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterNodeSshKeepalive(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a ssh-keepalive in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigKeepaliveInterConductorRouterServer(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the inter-conductor-router-server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-conductor-router-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigKeepaliveInterConductorRouterServer(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a inter-conductor-router-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-conductor-router-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNodeSshKeepaliveInter(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the inter-node configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNodeSshKeepaliveInter(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a inter-node in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSshKeepaliveInterNodeServer(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the inter-node-server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-node-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSshKeepaliveInterNodeServer(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a inter-node-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-node-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNodeSshKeepaliveInterRouter(configStore = 'running', router, node, callback)</td>
    <td style="padding:15px">Gets the inter-router configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNodeSshKeepaliveInterRouter(configStore = 'running', router, node, body, callback)</td>
    <td style="padding:15px">Modifies a inter-router in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/node/{pathv3}/ssh-keepalive/inter-router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterPathMtuDiscovery(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the path-mtu-discovery configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterPathMtuDiscovery(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a path-mtu-discovery in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/path-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterPeer(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the peer configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterPeer(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a peer resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterPeerClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing peer in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterPeer(configStore = 'running', router, peer, callback)</td>
    <td style="padding:15px">Gets the peer configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterPeer(configStore = 'running', router, peer, body, callback)</td>
    <td style="padding:15px">Modifies a peer in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterPeer(configStore = 'running', router, peer, callback)</td>
    <td style="padding:15px">Deletes a peer in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterPeerBfd(configStore = 'running', router, peer, callback)</td>
    <td style="padding:15px">Gets the bfd configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/{pathv3}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterPeerBfd(configStore = 'running', router, peer, body, callback)</td>
    <td style="padding:15px">Modifies a bfd in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/peer/{pathv3}/bfd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterRedundancyGroup(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the redundancy-group configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterRedundancyGroup(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a redundancy-group resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRedundancyGroupClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing redundancy-group in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRedundancyGroupRedundancygroup(configStore = 'running', router, redundancygroup, callback)</td>
    <td style="padding:15px">Gets the redundancy-group configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterRedundancyGroupRedundancygroup(configStore = 'running', router, redundancygroup, body, callback)</td>
    <td style="padding:15px">Modifies a redundancy-group in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterRedundancyGroupRedundancygroup(configStore = 'running', router, redundancygroup, callback)</td>
    <td style="padding:15px">Deletes a redundancy-group in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRedundancyGroupRedundancygroupMember(configStore = 'running', router, redundancygroup, callback)</td>
    <td style="padding:15px">Gets the member configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRedundancyGroupRedundancygroupMember(configStore = 'running', router, redundancygroup, body, callback)</td>
    <td style="padding:15px">Creates a member resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRedundancyGroupRedundancygroupMemberClone(configStore = 'running', router, redundancygroup, body, callback)</td>
    <td style="padding:15px">Clone an existing member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRedundancyGroupRedundancygroupMember(configStore = 'running', router, redundancygroup, member, callback)</td>
    <td style="padding:15px">Gets the member configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRedundancyGroupRedundancygroupMember(configStore = 'running', router, redundancygroup, member, body, callback)</td>
    <td style="padding:15px">Modifies a member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRedundancyGroupRedundancygroupMember(configStore = 'running', router, redundancygroup, member, callback)</td>
    <td style="padding:15px">Deletes a member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/redundancy-group/{pathv3}/member/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterRouting(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the routing configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterRouting(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a routing resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRoutingClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing routing in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRouting(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Gets the routing configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterRouting(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Modifies a routing in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterRouting(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Deletes a routing in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRoutingInterface(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Gets the interface configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRoutingInterface(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Creates a interface resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingInterfaceClone(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Clone an existing interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingInterface(configStore = 'running', router, routing, interfaceParam, callback)</td>
    <td style="padding:15px">Gets the interface configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingInterface(configStore = 'running', router, routing, interfaceParam, body, callback)</td>
    <td style="padding:15px">Modifies a interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingInterface(configStore = 'running', router, routing, interfaceParam, callback)</td>
    <td style="padding:15px">Deletes a interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/interface/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRoutingOspf(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Gets the ospf configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRoutingOspf(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Creates a ospf resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingOspfClone(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Clone an existing ospf in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingOspf(configStore = 'running', router, routing, ospf, callback)</td>
    <td style="padding:15px">Gets the ospf configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingOspf(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Modifies a ospf in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingOspf(configStore = 'running', router, routing, ospf, callback)</td>
    <td style="padding:15px">Deletes a ospf in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingOspfAdvertiseDefault(configStore = 'running', router, routing, ospf, callback)</td>
    <td style="padding:15px">Gets the advertise-default configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/advertise-default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingOspfAdvertiseDefault(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Modifies a advertise-default in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/advertise-default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingOspfArea(configStore = 'running', router, routing, ospf, callback)</td>
    <td style="padding:15px">Gets the area configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingOspfArea(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Creates a area resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigOspfAreaClone(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Clone an existing area in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigOspfArea(configStore = 'running', router, routing, ospf, area, callback)</td>
    <td style="padding:15px">Gets the area configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigOspfArea(configStore = 'running', router, routing, ospf, area, body, callback)</td>
    <td style="padding:15px">Modifies a area in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigOspfArea(configStore = 'running', router, routing, ospf, area, callback)</td>
    <td style="padding:15px">Deletes a area in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigOspfAreaInterface(configStore = 'running', router, routing, ospf, area, callback)</td>
    <td style="padding:15px">Gets the interface configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigOspfAreaInterface(configStore = 'running', router, routing, ospf, area, body, callback)</td>
    <td style="padding:15px">Creates a interface resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAreaInterfaceClone(configStore = 'running', router, routing, ospf, area, body, callback)</td>
    <td style="padding:15px">Clone an existing interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAreaInterface(configStore = 'running', router, routing, ospf, area, interfaceParam, callback)</td>
    <td style="padding:15px">Gets the interface configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAreaInterface(configStore = 'running', router, routing, ospf, area, interfaceParam, body, callback)</td>
    <td style="padding:15px">Modifies a interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAreaInterface(configStore = 'running', router, routing, ospf, area, interfaceParam, callback)</td>
    <td style="padding:15px">Deletes a interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/interface/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigOspfAreaSummaryRange(configStore = 'running', router, routing, ospf, area, callback)</td>
    <td style="padding:15px">Gets the summary-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigOspfAreaSummaryRange(configStore = 'running', router, routing, ospf, area, body, callback)</td>
    <td style="padding:15px">Creates a summary-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAreaSummaryRangeClone(configStore = 'running', router, routing, ospf, area, body, callback)</td>
    <td style="padding:15px">Clone an existing summary-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAreaSummaryRangeSummaryrange(configStore = 'running', router, routing, ospf, area, summaryrange, callback)</td>
    <td style="padding:15px">Gets the summary-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAreaSummaryRangeSummaryrange(configStore = 'running', router, routing, ospf, area, summaryrange, body, callback)</td>
    <td style="padding:15px">Modifies a summary-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAreaSummaryRangeSummaryrange(configStore = 'running', router, routing, ospf, area, summaryrange, callback)</td>
    <td style="padding:15px">Deletes a summary-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/area/{pathv5}/summary-range/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingOspfRedistribute(configStore = 'running', router, routing, ospf, callback)</td>
    <td style="padding:15px">Gets the redistribute configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingOspfRedistribute(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Creates a redistribute resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigOspfRedistributeClone(configStore = 'running', router, routing, ospf, body, callback)</td>
    <td style="padding:15px">Clone an existing redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigOspfRedistribute(configStore = 'running', router, routing, ospf, redistribute, callback)</td>
    <td style="padding:15px">Gets the redistribute configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigOspfRedistribute(configStore = 'running', router, routing, ospf, redistribute, body, callback)</td>
    <td style="padding:15px">Modifies a redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigOspfRedistribute(configStore = 'running', router, routing, ospf, redistribute, callback)</td>
    <td style="padding:15px">Deletes a redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/ospf/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRoutingProtocol(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Gets the routing-protocol configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRoutingProtocol(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Creates a routing-protocol resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolClone(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Clone an existing routing-protocol in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocol(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the routing-protocol configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingProtocolRoutingprotocol(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Modifies a routing-protocol in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingProtocolRoutingprotocol(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Deletes a routing-protocol in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolAddressFamily(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the address-family configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolRoutingprotocolAddressFamily(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Creates a address-family resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigProtocolRoutingprotocolAddressFamilyClone(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Clone an existing address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigProtocolRoutingprotocolAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the address-family configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigProtocolRoutingprotocolAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigProtocolRoutingprotocolAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Deletes a address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyAggregate(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the aggregate-address configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressFamilyAddressfamilyAggregate(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Creates a aggregate-address resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigFamilyAddressfamilyAggregateAddressClone(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Clone an existing aggregate-address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFamilyAddressfamilyAggregateAddressAggregateaddress(configStore = 'running', router, routing, routingprotocol, addressfamily, aggregateaddress, callback)</td>
    <td style="padding:15px">Gets the aggregate-address configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigFamilyAddressfamilyAggregateAddressAggregateaddress(configStore = 'running', router, routing, routingprotocol, addressfamily, aggregateaddress, body, callback)</td>
    <td style="padding:15px">Modifies a aggregate-address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigFamilyAddressfamilyAggregateAddressAggregateaddress(configStore = 'running', router, routing, routingprotocol, addressfamily, aggregateaddress, callback)</td>
    <td style="padding:15px">Deletes a aggregate-address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/aggregate-address/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFamilyAddressfamilyDefaultRouteDistance(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the default-route-distance configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/default-route-distance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigFamilyAddressfamilyDefaultRouteDistance(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a default-route-distance in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/default-route-distance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyGracefulRestart(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the graceful-restart configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/graceful-restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressFamilyAddressfamilyGracefulRestart(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a graceful-restart in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/graceful-restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolAddressFamilyAddressfamilyNetwork(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the network configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingprotocolAddressFamilyAddressfamilyNetwork(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Creates a network resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAddressFamilyAddressfamilyNetworkClone(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Clone an existing network in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyNetwork(configStore = 'running', router, routing, routingprotocol, addressfamily, network, callback)</td>
    <td style="padding:15px">Gets the network configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressFamilyAddressfamilyNetwork(configStore = 'running', router, routing, routingprotocol, addressfamily, network, body, callback)</td>
    <td style="padding:15px">Modifies a network in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAddressFamilyAddressfamilyNetwork(configStore = 'running', router, routing, routingprotocol, addressfamily, network, callback)</td>
    <td style="padding:15px">Deletes a network in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/network/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyPrefixLimit(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the prefix-limit configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/prefix-limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressFamilyAddressfamilyPrefixLimit(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a prefix-limit in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/prefix-limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFamilyAddressfamilyUseMultiplePaths(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the use-multiple-paths configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigFamilyAddressfamilyUseMultiplePaths(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a use-multiple-paths in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressfamilyUseMultiplePathsEbgp(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the ebgp configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths/ebgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressfamilyUseMultiplePathsEbgp(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a ebgp in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths/ebgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressfamilyUseMultiplePathsIbgp(configStore = 'running', router, routing, routingprotocol, addressfamily, callback)</td>
    <td style="padding:15px">Gets the ibgp configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths/ibgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressfamilyUseMultiplePathsIbgp(configStore = 'running', router, routing, routingprotocol, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a ibgp in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/address-family/{pathv5}/use-multiple-paths/ibgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolConfederation(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the confederation configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/confederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingProtocolRoutingprotocolConfederation(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Modifies a confederation in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/confederation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolNeighbor(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the neighbor configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolRoutingprotocolNeighbor(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Creates a neighbor resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolRoutingprotocolNeighborClone(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Clone an existing neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolNeighbor2(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the neighbor configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingProtocolRoutingprotocolNeighbor(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingProtocolRoutingprotocolNeighbor(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Deletes a neighbor in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolNeighborAddressFamily(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the address-family configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingprotocolNeighborAddressFamily(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Creates a address-family resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigNeighborAddressFamilyClone(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Clone an existing address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, callback)</td>
    <td style="padding:15px">Gets the address-family configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigNeighborAddressFamilyAddressfamily(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, callback)</td>
    <td style="padding:15px">Deletes a address-family in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFamilyAddressfamilyAsPathOptions(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, callback)</td>
    <td style="padding:15px">Gets the as-path-options configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/as-path-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigFamilyAddressfamilyAsPathOptions(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a as-path-options in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/as-path-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyPrefixLimit2(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, callback)</td>
    <td style="padding:15px">Gets the prefix-limit configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/prefix-limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressFamilyAddressfamilyPrefixLimit2(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a prefix-limit in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/prefix-limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAddressFamilyAddressfamilyRouteReflector(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, callback)</td>
    <td style="padding:15px">Gets the route-reflector configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/route-reflector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAddressFamilyAddressfamilyRouteReflector(configStore = 'running', router, routing, routingprotocol, neighbor, addressfamily, body, callback)</td>
    <td style="padding:15px">Modifies a route-reflector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/address-family/{pathv6}/route-reflector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolNeighborMultihop(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the multihop configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/multihop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingprotocolNeighborMultihop(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a multihop in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/multihop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolNeighborPolicy(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the neighbor-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/neighbor-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingprotocolNeighborPolicy(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a neighbor-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/neighbor-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolNeighborTimers(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the timers configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/timers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingprotocolNeighborTimers(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a timers in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/timers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingprotocolNeighborTransport(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingprotocolNeighborTransport(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigNeighborTransportLocalAddress(configStore = 'running', router, routing, routingprotocol, neighbor, callback)</td>
    <td style="padding:15px">Gets the local-address configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/transport/local-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigNeighborTransportLocalAddress(configStore = 'running', router, routing, routingprotocol, neighbor, body, callback)</td>
    <td style="padding:15px">Modifies a local-address in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/neighbor/{pathv5}/transport/local-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolRedistribute(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the redistribute configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolRoutingprotocolRedistribute(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Creates a redistribute resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingProtocolRoutingprotocolRedistributeClone(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Clone an existing redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolRedistribute2(configStore = 'running', router, routing, routingprotocol, redistribute, callback)</td>
    <td style="padding:15px">Gets the redistribute configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingProtocolRoutingprotocolRedistribute(configStore = 'running', router, routing, routingprotocol, redistribute, body, callback)</td>
    <td style="padding:15px">Modifies a redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingProtocolRoutingprotocolRedistribute(configStore = 'running', router, routing, routingprotocol, redistribute, callback)</td>
    <td style="padding:15px">Deletes a redistribute in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/redistribute/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigProtocolRoutingprotocolRouteSelectionOptions(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the route-selection-options configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/route-selection-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigProtocolRoutingprotocolRouteSelectionOptions(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Modifies a route-selection-options in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/route-selection-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingProtocolRoutingprotocolTimers(configStore = 'running', router, routing, routingprotocol, callback)</td>
    <td style="padding:15px">Gets the timers configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/timers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingProtocolRoutingprotocolTimers(configStore = 'running', router, routing, routingprotocol, body, callback)</td>
    <td style="padding:15px">Modifies a timers in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/routing-protocol/{pathv4}/timers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterRoutingStaticRoute(configStore = 'running', router, routing, callback)</td>
    <td style="padding:15px">Gets the static-route configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterRoutingStaticRoute(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Creates a static-route resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingStaticRouteClone(configStore = 'running', router, routing, body, callback)</td>
    <td style="padding:15px">Clone an existing static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingStaticRouteStaticroute(configStore = 'running', router, routing, staticroute, callback)</td>
    <td style="padding:15px">Gets the static-route configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRoutingStaticRouteStaticroute(configStore = 'running', router, routing, staticroute, body, callback)</td>
    <td style="padding:15px">Modifies a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRoutingStaticRouteStaticroute(configStore = 'running', router, routing, staticroute, callback)</td>
    <td style="padding:15px">Deletes a static-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouteStaticrouteNextHopInterface(configStore = 'running', router, routing, staticroute, callback)</td>
    <td style="padding:15px">Gets the next-hop-interface configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouteStaticrouteNextHopInterface(configStore = 'running', router, routing, staticroute, body, callback)</td>
    <td style="padding:15px">Creates a next-hop-interface resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStaticrouteNextHopInterfaceClone(configStore = 'running', router, routing, staticroute, body, callback)</td>
    <td style="padding:15px">Clone an existing next-hop-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStaticrouteNextHopInterfaceNexthopinterface(configStore = 'running', router, routing, staticroute, nexthopinterface, callback)</td>
    <td style="padding:15px">Gets the next-hop-interface configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStaticrouteNextHopInterfaceNexthopinterface(configStore = 'running', router, routing, staticroute, nexthopinterface, body, callback)</td>
    <td style="padding:15px">Modifies a next-hop-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStaticrouteNextHopInterfaceNexthopinterface(configStore = 'running', router, routing, staticroute, nexthopinterface, callback)</td>
    <td style="padding:15px">Deletes a next-hop-interface in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/routing/{pathv3}/static-route/{pathv4}/next-hop-interface/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterServiceRoute(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the service-route configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRouterServiceRoute(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a service-route resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterServiceRoutePolicy(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the service-route-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterServiceRoutePolicy(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a service-route-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterServiceRoutePolicyClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing service-route-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterServiceRoutePolicyServiceroutepolicy(configStore = 'running', router, serviceroutepolicy, callback)</td>
    <td style="padding:15px">Gets the service-route-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterServiceRoutePolicyServiceroutepolicy(configStore = 'running', router, serviceroutepolicy, body, callback)</td>
    <td style="padding:15px">Modifies a service-route-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterServiceRoutePolicyServiceroutepolicy(configStore = 'running', router, serviceroutepolicy, callback)</td>
    <td style="padding:15px">Deletes a service-route-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterServiceRouteClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing service-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterServiceRouteServiceroute(configStore = 'running', router, serviceroute, callback)</td>
    <td style="padding:15px">Gets the service-route configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterServiceRouteServiceroute(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Modifies a service-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterServiceRouteServiceroute(configStore = 'running', router, serviceroute, callback)</td>
    <td style="padding:15px">Deletes a service-route in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterServiceRouteServicerouteHost(configStore = 'running', router, serviceroute, callback)</td>
    <td style="padding:15px">Gets the host configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterServiceRouteServicerouteHost(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Creates a host resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceRouteServicerouteHostClone(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Clone an existing host in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceRouteServicerouteHost(configStore = 'running', router, serviceroute, host, callback)</td>
    <td style="padding:15px">Gets the host configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceRouteServicerouteHost(configStore = 'running', router, serviceroute, host, body, callback)</td>
    <td style="padding:15px">Modifies a host in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceRouteServicerouteHost(configStore = 'running', router, serviceroute, host, callback)</td>
    <td style="padding:15px">Deletes a host in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/host/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceRouteServicerouteNextHop(configStore = 'running', router, serviceroute, callback)</td>
    <td style="padding:15px">Gets the next-hop configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceRouteServicerouteNextHop(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Creates a next-hop resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouteServicerouteNextHopClone(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Clone an existing next-hop in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouteServicerouteNextHopNexthop(configStore = 'running', router, serviceroute, nexthop, callback)</td>
    <td style="padding:15px">Gets the next-hop configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouteServicerouteNextHopNexthop(configStore = 'running', router, serviceroute, nexthop, body, callback)</td>
    <td style="padding:15px">Modifies a next-hop in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouteServicerouteNextHopNexthop(configStore = 'running', router, serviceroute, nexthop, callback)</td>
    <td style="padding:15px">Deletes a next-hop in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/next-hop/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceRouteServicerouteReachabilityDetection(configStore = 'running', router, serviceroute, callback)</td>
    <td style="padding:15px">Gets the reachability-detection configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/reachability-detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceRouteServicerouteReachabilityDetection(configStore = 'running', router, serviceroute, body, callback)</td>
    <td style="padding:15px">Modifies a reachability-detection in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/service-route/{pathv3}/reachability-detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterSystem(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the system configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRouterSystem(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a system in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemAudit(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the audit configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemAudit(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a audit in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemAuditAdministration(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the administration configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/administration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemAuditAdministration(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a administration in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/administration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemAudit2(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the system configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemAudit2(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a system in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemAuditTraffic(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the traffic configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemAuditTraffic(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a traffic in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/audit/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemLocalLogin(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the local-login configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/local-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemLocalLogin(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a local-login in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/local-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemLocalLoginNetconf(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the netconf configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/local-login/netconf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemLocalLoginNetconf(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a netconf in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/local-login/netconf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemLogCategory(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the log-category configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterSystemLogCategory(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a log-category resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterSystemLogCategoryClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing log-category in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemLogCategoryLogcategory(configStore = 'running', router, logcategory, callback)</td>
    <td style="padding:15px">Gets the log-category configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemLogCategoryLogcategory(configStore = 'running', router, logcategory, body, callback)</td>
    <td style="padding:15px">Modifies a log-category in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigRouterSystemLogCategoryLogcategory(configStore = 'running', router, logcategory, callback)</td>
    <td style="padding:15px">Deletes a log-category in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/log-category/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemMetrics(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the metrics configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemMetrics(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a metrics in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemNtp(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the ntp configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemNtp(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a ntp in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemNtpServer(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the server configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterSystemNtpServer(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a server resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSystemNtpServerClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSystemNtpServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Gets the server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSystemNtpServer(configStore = 'running', router, server, body, callback)</td>
    <td style="padding:15px">Modifies a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigSystemNtpServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Deletes a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/ntp/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemRemoteLogin(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the remote-login configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/remote-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemRemoteLogin(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a remote-login in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/remote-login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemServices(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the services configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemServices(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a services in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemServicesSnmpServer(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the snmp-server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemServicesSnmpServer(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a snmp-server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServicesSnmpServerAccessControl(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the access-control configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicesSnmpServerAccessControl(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a access-control resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSnmpServerAccessControlClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing access-control in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSnmpServerAccessControlAccesscontrol(configStore = 'running', router, accesscontrol, callback)</td>
    <td style="padding:15px">Gets the access-control configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSnmpServerAccessControlAccesscontrol(configStore = 'running', router, accesscontrol, body, callback)</td>
    <td style="padding:15px">Modifies a access-control in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigSnmpServerAccessControlAccesscontrol(configStore = 'running', router, accesscontrol, callback)</td>
    <td style="padding:15px">Deletes a access-control in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/access-control/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServicesSnmpServerNotificationReceiver(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the notification-receiver configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicesSnmpServerNotificationReceiver(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a notification-receiver resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSnmpServerNotificationReceiverClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing notification-receiver in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSnmpServerNotificationReceiverNotificationreceiver(configStore = 'running', router, notificationreceiver, callback)</td>
    <td style="padding:15px">Gets the notification-receiver configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSnmpServerNotificationReceiverNotificationreceiver(configStore = 'running', router, notificationreceiver, body, callback)</td>
    <td style="padding:15px">Modifies a notification-receiver in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigSnmpServerNotificationReceiverNotificationreceiver(configStore = 'running', router, notificationreceiver, callback)</td>
    <td style="padding:15px">Deletes a notification-receiver in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/snmp-server/notification-receiver/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemServicesWebserver(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the webserver configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemServicesWebserver(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a webserver in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSystemServicesWebserverServer(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the server configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSystemServicesWebserverServer(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a server resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicesWebserverServerClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServicesWebserverServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Gets the server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServicesWebserverServer(configStore = 'running', router, server, body, callback)</td>
    <td style="padding:15px">Modifies a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServicesWebserverServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Deletes a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/services/webserver/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemSoftwareUpdate(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the software-update configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/software-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemSoftwareUpdate(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a software-update in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/software-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemSoftwareUpdateRepository(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the repository configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/software-update/repository?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemSoftwareUpdateRepository(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a repository in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/software-update/repository?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemSyslog(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the syslog configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigRouterSystemSyslog(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a syslog in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRouterSystemSyslogServer(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the server configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRouterSystemSyslogServer(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Creates a server resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSystemSyslogServerClone(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Clone an existing server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSystemSyslogServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Gets the server configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSystemSyslogServer(configStore = 'running', router, server, body, callback)</td>
    <td style="padding:15px">Modifies a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigSystemSyslogServer(configStore = 'running', router, server, callback)</td>
    <td style="padding:15px">Deletes a server in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/system/syslog/server/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRouterUdpTransform(configStore = 'running', router, callback)</td>
    <td style="padding:15px">Gets the udp-transform configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRouterUdpTransform(configStore = 'running', router, body, callback)</td>
    <td style="padding:15px">Modifies a udp-transform in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/router/{pathv2}/udp-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRouting(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the routing configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityRouting(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Modifies a routing in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRoutingFilter(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the filter configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityRoutingFilter(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a filter resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRoutingFilterClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing filter in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRoutingFilter(configStore = 'running', filter, callback)</td>
    <td style="padding:15px">Gets the filter configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRoutingFilter(configStore = 'running', filter, body, callback)</td>
    <td style="padding:15px">Modifies a filter in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAuthorityRoutingFilter(configStore = 'running', filter, callback)</td>
    <td style="padding:15px">Deletes a filter in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingFilterRule(configStore = 'running', filter, callback)</td>
    <td style="padding:15px">Gets the rule configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingFilterRule(configStore = 'running', filter, body, callback)</td>
    <td style="padding:15px">Creates a rule resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigFilterRuleClone(configStore = 'running', filter, body, callback)</td>
    <td style="padding:15px">Clone an existing rule in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigFilterRuleMove(configStore = 'running', filter, body, callback)</td>
    <td style="padding:15px">Move a rule to a new position in the list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFilterRule(configStore = 'running', filter, rule, callback)</td>
    <td style="padding:15px">Gets the rule configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigFilterRule(configStore = 'running', filter, rule, body, callback)</td>
    <td style="padding:15px">Modifies a rule in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigFilterRule(configStore = 'running', filter, rule, callback)</td>
    <td style="padding:15px">Deletes a rule in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/filter/{pathv2}/rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityRoutingPolicy(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityRoutingPolicy(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityRoutingPolicyClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityRoutingPolicy(configStore = 'running', policy, callback)</td>
    <td style="padding:15px">Gets the policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityRoutingPolicy(configStore = 'running', policy, body, callback)</td>
    <td style="padding:15px">Modifies a policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigAuthorityRoutingPolicy(configStore = 'running', policy, callback)</td>
    <td style="padding:15px">Deletes a policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigRoutingPolicyStatement(configStore = 'running', policy, callback)</td>
    <td style="padding:15px">Gets the statement configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRoutingPolicyStatement(configStore = 'running', policy, body, callback)</td>
    <td style="padding:15px">Creates a statement resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPolicyStatementClone(configStore = 'running', policy, body, callback)</td>
    <td style="padding:15px">Clone an existing statement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPolicyStatementMove(configStore = 'running', policy, body, callback)</td>
    <td style="padding:15px">Move a statement to a new position in the list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPolicyStatement(configStore = 'running', policy, statement, callback)</td>
    <td style="padding:15px">Gets the statement configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPolicyStatement(configStore = 'running', policy, statement, body, callback)</td>
    <td style="padding:15px">Modifies a statement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPolicyStatement(configStore = 'running', policy, statement, callback)</td>
    <td style="padding:15px">Deletes a statement in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPolicyStatementAction(configStore = 'running', policy, statement, callback)</td>
    <td style="padding:15px">Gets the action configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPolicyStatementAction(configStore = 'running', policy, statement, body, callback)</td>
    <td style="padding:15px">Creates a action resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStatementActionClone(configStore = 'running', policy, statement, body, callback)</td>
    <td style="padding:15px">Clone an existing action in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStatementAction(configStore = 'running', policy, statement, action, callback)</td>
    <td style="padding:15px">Gets the action configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStatementAction(configStore = 'running', policy, statement, action, body, callback)</td>
    <td style="padding:15px">Modifies a action in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStatementAction(configStore = 'running', policy, statement, action, callback)</td>
    <td style="padding:15px">Deletes a action in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/action/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPolicyStatementCondition(configStore = 'running', policy, statement, callback)</td>
    <td style="padding:15px">Gets the condition configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPolicyStatementCondition(configStore = 'running', policy, statement, body, callback)</td>
    <td style="padding:15px">Creates a condition resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStatementConditionClone(configStore = 'running', policy, statement, body, callback)</td>
    <td style="padding:15px">Clone an existing condition in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStatementCondition(configStore = 'running', policy, statement, condition, callback)</td>
    <td style="padding:15px">Gets the condition configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStatementCondition(configStore = 'running', policy, statement, condition, body, callback)</td>
    <td style="padding:15px">Modifies a condition in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStatementCondition(configStore = 'running', policy, statement, condition, callback)</td>
    <td style="padding:15px">Deletes a condition in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/routing/policy/{pathv2}/statement/{pathv3}/condition/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthoritySecurity(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the security configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthoritySecurity(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a security resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthoritySecurityClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing security in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthoritySecurity2(configStore = 'running', security, callback)</td>
    <td style="padding:15px">Gets the security configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthoritySecurity(configStore = 'running', security, body, callback)</td>
    <td style="padding:15px">Modifies a security in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthoritySecurity(configStore = 'running', security, callback)</td>
    <td style="padding:15px">Deletes a security in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/security/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityService(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the service configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityService(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a service resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityServiceClass(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the service-class configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityServiceClass(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a service-class resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityServiceClassClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing service-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityServiceClassServiceclass(configStore = 'running', serviceclass, callback)</td>
    <td style="padding:15px">Gets the service-class configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityServiceClassServiceclass(configStore = 'running', serviceclass, body, callback)</td>
    <td style="padding:15px">Modifies a service-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityServiceClassServiceclass(configStore = 'running', serviceclass, callback)</td>
    <td style="padding:15px">Deletes a service-class in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-class/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityServicePolicy(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the service-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityServicePolicy(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a service-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityServicePolicyClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing service-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityServicePolicyServicepolicy(configStore = 'running', servicepolicy, callback)</td>
    <td style="padding:15px">Gets the service-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityServicePolicyServicepolicy(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Modifies a service-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityServicePolicyServicepolicy(configStore = 'running', servicepolicy, callback)</td>
    <td style="padding:15px">Deletes a service-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServicePolicyServicepolicyAppliesTo(configStore = 'running', servicepolicy, callback)</td>
    <td style="padding:15px">Gets the applies-to configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicePolicyServicepolicyAppliesTo(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Creates a applies-to resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigPolicyServicepolicyAppliesToClone(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Clone an existing applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPolicyServicepolicyAppliesToAppliesto(configStore = 'running', servicepolicy, appliesto, callback)</td>
    <td style="padding:15px">Gets the applies-to configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigPolicyServicepolicyAppliesToAppliesto(configStore = 'running', servicepolicy, appliesto, body, callback)</td>
    <td style="padding:15px">Modifies a applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigPolicyServicepolicyAppliesToAppliesto(configStore = 'running', servicepolicy, appliesto, callback)</td>
    <td style="padding:15px">Deletes a applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityServicePolicyServicepolicyVector(configStore = 'running', servicepolicy, callback)</td>
    <td style="padding:15px">Gets the vector configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityServicePolicyServicepolicyVector(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Creates a vector resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicePolicyServicepolicyVectorClone(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Clone an existing vector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServicePolicyServicepolicyVectorMove(configStore = 'running', servicepolicy, body, callback)</td>
    <td style="padding:15px">Move a vector to a new position in the list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServicePolicyServicepolicyVector(configStore = 'running', servicepolicy, vector, callback)</td>
    <td style="padding:15px">Gets the vector configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServicePolicyServicepolicyVector(configStore = 'running', servicepolicy, vector, body, callback)</td>
    <td style="padding:15px">Modifies a vector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServicePolicyServicepolicyVector(configStore = 'running', servicepolicy, vector, callback)</td>
    <td style="padding:15px">Deletes a vector in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service-policy/{pathv2}/vector/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityServiceClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityService2(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Gets the service configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityService(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Modifies a service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityService(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Deletes a service in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityServiceAccessPolicy(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityServiceAccessPolicy(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Creates a access-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceAccessPolicyClone(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Clone an existing access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceAccessPolicyAccesspolicy(configStore = 'running', service, accesspolicy, callback)</td>
    <td style="padding:15px">Gets the access-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceAccessPolicyAccesspolicy(configStore = 'running', service, accesspolicy, body, callback)</td>
    <td style="padding:15px">Modifies a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceAccessPolicyAccesspolicy(configStore = 'running', service, accesspolicy, callback)</td>
    <td style="padding:15px">Deletes a access-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/access-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityServiceAppliesTo(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Gets the applies-to configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityServiceAppliesTo(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Creates a applies-to resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceAppliesToClone(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Clone an existing applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceAppliesToAppliesto(configStore = 'running', service, appliesto, callback)</td>
    <td style="padding:15px">Gets the applies-to configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceAppliesToAppliesto(configStore = 'running', service, appliesto, body, callback)</td>
    <td style="padding:15px">Modifies a applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceAppliesToAppliesto(configStore = 'running', service, appliesto, callback)</td>
    <td style="padding:15px">Deletes a applies-to in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/applies-to/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceMulticastSenderPolicy(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Gets the multicast-sender-policy configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceMulticastSenderPolicy(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Creates a multicast-sender-policy resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceMulticastSenderPolicyClone(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Clone an existing multicast-sender-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceMulticastSenderPolicyMulticastsenderpolicy(configStore = 'running', service, multicastsenderpolicy, callback)</td>
    <td style="padding:15px">Gets the multicast-sender-policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceMulticastSenderPolicyMulticastsenderpolicy(configStore = 'running', service, multicastsenderpolicy, body, callback)</td>
    <td style="padding:15px">Modifies a multicast-sender-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceMulticastSenderPolicyMulticastsenderpolicy(configStore = 'running', service, multicastsenderpolicy, callback)</td>
    <td style="padding:15px">Deletes a multicast-sender-policy in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/multicast-sender-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityServiceTransport(configStore = 'running', service, callback)</td>
    <td style="padding:15px">Gets the transport configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityServiceTransport(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Creates a transport resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceTransportClone(configStore = 'running', service, body, callback)</td>
    <td style="padding:15px">Clone an existing transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceTransport(configStore = 'running', service, transport, callback)</td>
    <td style="padding:15px">Gets the transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigServiceTransport(configStore = 'running', service, transport, body, callback)</td>
    <td style="padding:15px">Modifies a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigServiceTransport(configStore = 'running', service, transport, callback)</td>
    <td style="padding:15px">Deletes a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigServiceTransportPortRange(configStore = 'running', service, transport, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigServiceTransportPortRange(configStore = 'running', service, transport, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTransportPortRangeClone3(configStore = 'running', service, transport, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTransportPortRangePortrange3(configStore = 'running', service, transport, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTransportPortRangePortrange3(configStore = 'running', service, transport, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigTransportPortRangePortrange3(configStore = 'running', service, transport, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/service/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthoritySessionType(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the session-type configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthoritySessionType(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a session-type resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthoritySessionTypeClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing session-type in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthoritySessionTypeSessiontype(configStore = 'running', sessiontype, callback)</td>
    <td style="padding:15px">Gets the session-type configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthoritySessionTypeSessiontype(configStore = 'running', sessiontype, body, callback)</td>
    <td style="padding:15px">Modifies a session-type in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthoritySessionTypeSessiontype(configStore = 'running', sessiontype, callback)</td>
    <td style="padding:15px">Deletes a session-type in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthoritySessionTypeSessiontypeTransport(configStore = 'running', sessiontype, callback)</td>
    <td style="padding:15px">Gets the transport configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthoritySessionTypeSessiontypeTransport(configStore = 'running', sessiontype, body, callback)</td>
    <td style="padding:15px">Creates a transport resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSessionTypeSessiontypeTransportClone(configStore = 'running', sessiontype, body, callback)</td>
    <td style="padding:15px">Clone an existing transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSessionTypeSessiontypeTransport(configStore = 'running', sessiontype, transport, callback)</td>
    <td style="padding:15px">Gets the transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigSessionTypeSessiontypeTransport(configStore = 'running', sessiontype, transport, body, callback)</td>
    <td style="padding:15px">Modifies a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigSessionTypeSessiontypeTransport(configStore = 'running', sessiontype, transport, callback)</td>
    <td style="padding:15px">Deletes a transport in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSessiontypeTransportPortRange(configStore = 'running', sessiontype, transport, callback)</td>
    <td style="padding:15px">Gets the port-range configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSessiontypeTransportPortRange(configStore = 'running', sessiontype, transport, body, callback)</td>
    <td style="padding:15px">Creates a port-range resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTransportPortRangeClone4(configStore = 'running', sessiontype, transport, body, callback)</td>
    <td style="padding:15px">Clone an existing port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTransportPortRangePortrange4(configStore = 'running', sessiontype, transport, portrange, callback)</td>
    <td style="padding:15px">Gets the port-range configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTransportPortRangePortrange4(configStore = 'running', sessiontype, transport, portrange, body, callback)</td>
    <td style="padding:15px">Modifies a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigTransportPortRangePortrange4(configStore = 'running', sessiontype, transport, portrange, callback)</td>
    <td style="padding:15px">Deletes a port-range in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/session-type/{pathv2}/transport/{pathv3}/port-range/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityStepRepo(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the step-repo configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityStepRepo(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a step-repo resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityStepRepoClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing step-repo in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityStepRepoSteprepo(configStore = 'running', steprepo, callback)</td>
    <td style="padding:15px">Gets the step-repo configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityStepRepoSteprepo(configStore = 'running', steprepo, body, callback)</td>
    <td style="padding:15px">Modifies a step-repo in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityStepRepoSteprepo(configStore = 'running', steprepo, callback)</td>
    <td style="padding:15px">Deletes a step-repo in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/step-repo/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityTenant(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the tenant configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityTenant(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a tenant resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityTenantClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing tenant in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityTenant2(configStore = 'running', tenant, callback)</td>
    <td style="padding:15px">Gets the tenant configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityTenant(configStore = 'running', tenant, body, callback)</td>
    <td style="padding:15px">Modifies a tenant in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityTenant(configStore = 'running', tenant, callback)</td>
    <td style="padding:15px">Deletes a tenant in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityTenantMember(configStore = 'running', tenant, callback)</td>
    <td style="padding:15px">Gets the member configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigAuthorityTenantMember(configStore = 'running', tenant, body, callback)</td>
    <td style="padding:15px">Creates a member resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigTenantMemberClone(configStore = 'running', tenant, body, callback)</td>
    <td style="padding:15px">Clone an existing member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTenantMember(configStore = 'running', tenant, member, callback)</td>
    <td style="padding:15px">Gets the member configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTenantMember(configStore = 'running', tenant, member, body, callback)</td>
    <td style="padding:15px">Modifies a member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigTenantMember(configStore = 'running', tenant, member, callback)</td>
    <td style="padding:15px">Deletes a member in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/tenant/{pathv2}/member/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityTrafficProfile(configStore = 'running', callback)</td>
    <td style="padding:15px">Gets the traffic-profile configuration list</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityTrafficProfile(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Creates a traffic-profile resource in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigStoreAuthorityTrafficProfileClone(configStore = 'running', body, callback)</td>
    <td style="padding:15px">Clone an existing traffic-profile in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigStoreAuthorityTrafficProfileTrafficprofile(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Gets the traffic-profile configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigStoreAuthorityTrafficProfileTrafficprofile(configStore = 'running', trafficprofile, body, callback)</td>
    <td style="padding:15px">Modifies a traffic-profile in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigStoreAuthorityTrafficProfileTrafficprofile(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Deletes a traffic-profile in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigTrafficProfileTrafficprofileBestEffort(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Gets the best-effort configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/best-effort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigTrafficProfileTrafficprofileBestEffort(configStore = 'running', trafficprofile, body, callback)</td>
    <td style="padding:15px">Modifies a best-effort in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/best-effort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityTrafficProfileTrafficprofileHigh(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Gets the high configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/high?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityTrafficProfileTrafficprofileHigh(configStore = 'running', trafficprofile, body, callback)</td>
    <td style="padding:15px">Modifies a high in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/high?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityTrafficProfileTrafficprofileLow(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Gets the low configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/low?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityTrafficProfileTrafficprofileLow(configStore = 'running', trafficprofile, body, callback)</td>
    <td style="padding:15px">Modifies a low in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/low?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigAuthorityTrafficProfileTrafficprofileMedium(configStore = 'running', trafficprofile, callback)</td>
    <td style="padding:15px">Gets the medium configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/medium?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigAuthorityTrafficProfileTrafficprofileMedium(configStore = 'running', trafficprofile, body, callback)</td>
    <td style="padding:15px">Modifies a medium in the configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/{pathv1}/authority/traffic-profile/{pathv2}/medium?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogin(body, callback)</td>
    <td style="padding:15px">Authenticates a user.</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsAvailable(node, callback)</td>
    <td style="padding:15px">Gets the available plugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsInstall(body, callback)</td>
    <td style="padding:15px">Install a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsInstalled(node, callback)</td>
    <td style="padding:15px">Gets the installed plugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins/installed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsRemove(body, callback)</td>
    <td style="padding:15px">Remove a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouter(callback)</td>
    <td style="padding:15px">Gets a list of routers.</td>
    <td style="padding:15px">{base_path}/{version}/router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouter2(router, callback)</td>
    <td style="padding:15px">Gets a specific router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetrics(router, body, callback)</td>
    <td style="padding:15px">Gets timeseries metrics for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterTopSessions(router, orderBy, nodes, time, callback)</td>
    <td style="padding:15px">Gets the top sessions for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/topSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterTopSources(router, orderBy, first, callback)</td>
    <td style="padding:15px">Gets the top sources for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/topSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNode(router, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding nodes for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNode2(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the specified node within the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeAdjacency(router, node, callback)</td>
    <td style="padding:15px">Gets the corresponding adjacencies for the supplied router and node.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/adjacency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNetworkInterfaceAdjacency(router, node, deviceInterface, networkInterface, callback)</td>
    <td style="padding:15px">Gets the corresponding adjacencies for the supplied router, node, device interface, and network interface.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/deviceInterface/{pathv3}/networkInterface/{pathv4}/adjacency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeDeviceInterface(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding device interfaces for the supplied router and node.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/deviceInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterInterfaceDeviceNetwork(router, node, deviceInterface, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding network interfaces for the supplied router, node, and device interface.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/deviceInterface/{pathv3}/networkInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeNetworkInterface(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding network interfaces for the supplied router and node.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/networkInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNetworkInterfaceByDevice(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding network interfaces for the supplied router and node, grouped by their associated device interface.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/networkInterface/byDeviceInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeStatus(router, node, callback)</td>
    <td style="padding:15px">Gets status of a 128T node.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeVersion(router, node, callback)</td>
    <td style="padding:15px">Gets 128T software version information.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystem(callback)</td>
    <td style="padding:15px">Gets the current systems information.</td>
    <td style="padding:15px">{base_path}/{version}/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeTrafficFib(router, node, first, after, filter, callback)</td>
    <td style="padding:15px">Gets the current fib table.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/traffic/fib?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeTrafficFlows(router, node, first, after, filter, callback)</td>
    <td style="padding:15px">Gets the current flow table.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/traffic/flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeTrafficRib(router, node, first, after, detailed, filter, callback)</td>
    <td style="padding:15px">Gets the current rib table.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/traffic/rib?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterNodeTrafficSourceTenant(router, node, first, after, filter, callback)</td>
    <td style="padding:15px">Gets the current source tenant table.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/node/{pathv2}/traffic/sourceTenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterServiceRoutePolicy(router, callback)</td>
    <td style="padding:15px">Gets the corresponding service route policies for the supplied router.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/serviceRoutePolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAccessPolicyMeter(router, body, callback)</td>
    <td style="padding:15px">Access Policy table meter</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/access-policy/meter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsActiveSources(router, body, callback)</td>
    <td style="padding:15px">Active Sources</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/active-sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionDeviceInterfaceBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionDeviceInterfacePackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfacePacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfacePacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfacePacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfacePacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfacePacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfacePacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfacePacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfacePacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceSessionArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceSessionDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionDeviceInterfaceTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDeviceInterfaceTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/device-interface/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNetworkInterfaceBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNetworkInterfacePackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfacePacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfacePacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfacePacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfacePacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfacePacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfacePacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfacePacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfacePacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceSessionArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceSessionDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNetworkInterfaceTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNetworkInterfaceTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/network-interface/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionNodeBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeGrossEntitlement(router, body, callback)</td>
    <td style="padding:15px">Entitlement Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/gross-entitlement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionNodePackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodePacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodePacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodePacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodePacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodePacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodePacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodePacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodePacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionNodeTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionNodeTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/node/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceClassBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceClassPackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassPacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassPacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassPacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassPacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassPacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassPacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassPacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassPacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassSessionArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassSessionDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceClassTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceClassTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-class/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceGroupBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceGroupPackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupPacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupPacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupPacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupPacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupPacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupPacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupPacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupPacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupSessionArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupSessionDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceGroupTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceGroupTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-group/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceRouteBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceRoutePackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRoutePacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRoutePacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRoutePacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRoutePacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRoutePacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRoutePacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRoutePacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRoutePacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteSessionArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteSessionDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceRouteTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceRouteTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service-route/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionServiceBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionServicePackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServicePacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServicePacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServicePacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServicePacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServicePacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServicePacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServicePacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServicePacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionServiceTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionServiceTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/service/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionTenantBandwidth(router, body, callback)</td>
    <td style="padding:15px">Session Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantBandwidthReceived(router, body, callback)</td>
    <td style="padding:15px">Received Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantBandwidthReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantBandwidthReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantBandwidthTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantBandwidthTransmitted(router, body, callback)</td>
    <td style="padding:15px">Sent Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantBandwidthTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantBandwidthTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Bandwidth Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantBandwidthUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/bandwidth-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAggregateSessionTenantPackets(router, body, callback)</td>
    <td style="padding:15px">Total Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantPacketsReceived(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantPacketsReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-received-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantPacketsReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-received-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantPacketsTcp(router, body, callback)</td>
    <td style="padding:15px">Total TCP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantPacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantPacketsTransmittedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-transmitted-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantPacketsTransmittedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-transmitted-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantPacketsUdp(router, body, callback)</td>
    <td style="padding:15px">Total UDP Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/packets-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantArrivalRate(router, body, callback)</td>
    <td style="padding:15px">Session Arrival Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/session-arrival-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantCount(router, body, callback)</td>
    <td style="padding:15px">Session Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/session-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantDepartureRate(router, body, callback)</td>
    <td style="padding:15px">Session Departure Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/session-departure-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAggregateSessionTenantTcpRetransmissions(router, body, callback)</td>
    <td style="padding:15px">Total TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/tcp-retransmissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantTcpRetransmissionsReceived(router, body, callback)</td>
    <td style="padding:15px">Reverse TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/tcp-retransmissions-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionTenantTcpRetransmissionsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Forward TCP Data Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/aggregate-session/tenant/tcp-retransmissions-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAppIdRefreshes(router, body, callback)</td>
    <td style="padding:15px">app-id modules refreshes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/app-id/refreshes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsAppIdRetries(router, body, callback)</td>
    <td style="padding:15px">app-id modules retries</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/app-id/retries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsApplicationFrameworkInit(router, body, callback)</td>
    <td style="padding:15px">Init Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/application-framework/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsApplicationFrameworkShutdown(router, body, callback)</td>
    <td style="padding:15px">Shutdown Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/application-framework/shutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsApplicationFrameworkStart(router, body, callback)</td>
    <td style="padding:15px">Start Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/application-framework/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsApplicationFrameworkState(router, body, callback)</td>
    <td style="padding:15px">State Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/application-framework/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsApplicationFrameworkStop(router, body, callback)</td>
    <td style="padding:15px">Stop Count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/application-framework/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpEncapsulationSentFailure(router, body, callback)</td>
    <td style="padding:15px">IP Packets that failed to be sent after successful ARP resolution</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/encapsulation/sent/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpEncapsulationSentSuccess(router, body, callback)</td>
    <td style="padding:15px">IP Packets sent after successful ARP resolution</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/encapsulation/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpEntries(router, body, callback)</td>
    <td style="padding:15px">Active ARP entries</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpQueuedFailureDrop(router, body, callback)</td>
    <td style="padding:15px">Queued Packet Drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/queued/failure/drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpQueuedFailureQueueFull(router, body, callback)</td>
    <td style="padding:15px">Packet queue full</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/queued/failure/queue-full?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpQueuedPackets(router, body, callback)</td>
    <td style="padding:15px">Queued packets waiting for valid ARP entry</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/queued/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpReceivedReply(router, body, callback)</td>
    <td style="padding:15px">ARP Replies Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/arp-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpReceivedRequest(router, body, callback)</td>
    <td style="padding:15px">ARP Requests Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/arp-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsReply(router, body, callback)</td>
    <td style="padding:15px">Errors processing ARP Replies</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/arp-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsRequest(router, body, callback)</td>
    <td style="padding:15px">Errors processing ARP Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/arp-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Errors processing ICMPv6 Neighbor Advertisements</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsNeighborSolicit(router, body, callback)</td>
    <td style="padding:15px">Errors processing ICMPv6 Neighbor Solicits</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/neighbor-solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsProcessing(router, body, callback)</td>
    <td style="padding:15px">ARP Receive Processing Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/processing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedErrorsUnknownType(router, body, callback)</td>
    <td style="padding:15px">ARP Received Unknown packet type</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/errors/unknown-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpReceivedGratuitous(router, body, callback)</td>
    <td style="padding:15px">Gratuitous ARP Replies Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/gratuitous-arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpReceivedNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Neighbor Advertisements Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsArpReceivedNeighborSolicit(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Neighbor Solicits Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/neighbor-solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedOffSubnetArpRequest(router, body, callback)</td>
    <td style="padding:15px">Off-subnet ARP Requests Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/off-subnet-arp-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedOffSubnetNeighborSolicit(router, body, callback)</td>
    <td style="padding:15px">Off-subnet ICMPv6 Neighbor Solicits Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/off-subnet-neighbor-solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpReceivedUnsolicitedNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Unsolicited ICMPv6 Neighbor Advertisements Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/received/unsolicited-neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureAllocation(router, body, callback)</td>
    <td style="padding:15px">ARP Packet Allocation Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureReply(router, body, callback)</td>
    <td style="padding:15px">Errors sending ARP Replies</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/arp-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureRequest(router, body, callback)</td>
    <td style="padding:15px">Errors sending ARP Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/arp-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureGratuitous(router, body, callback)</td>
    <td style="padding:15px">Errors sending Gratuitous ARPs</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/gratuitous-arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentFailureMulticastListenerReport(router, body, callback)</td>
    <td style="padding:15px">Errors sending ICMPv6 Multicast Listener Report</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/multicast-listener-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Errors sending ICMPv6 Neighbor Advertisements</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureNeighborSolicit(router, body, callback)</td>
    <td style="padding:15px">Errors sending ICMPv6 Neighbor Solicits</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/neighbor-solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentFailureStandby(router, body, callback)</td>
    <td style="padding:15px">ARP Packets discarded on standby interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/standby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentFailureUnsolicitedNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Errors sending ICMPv6 Unsolicited Neighbor Advertisements</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/failure/unsolicited-neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentSuccessReply(router, body, callback)</td>
    <td style="padding:15px">ARP Replies Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/arp-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentSuccessRequest(router, body, callback)</td>
    <td style="padding:15px">ARP Requests Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/arp-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentSuccessGratuitous(router, body, callback)</td>
    <td style="padding:15px">Gratuitous ARPs Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/gratuitous-arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentSuccessInSubnetProxy(router, body, callback)</td>
    <td style="padding:15px">In-subnet ARP-proxy Responses Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/in-subnet-proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentSuccessMulticastListenerReport(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Multicast Listener Reports Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/multicast-listener-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentSuccessNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Neighbor Advertisements Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterArpSentSuccessNeighborSolicit(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Neighbor Solicits Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/neighbor-solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentSuccessOffSubnetProxy(router, body, callback)</td>
    <td style="padding:15px">Off-subnet ARP-proxy Responses Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/off-subnet-proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentSuccessUnsolicitedNeighborAdvertisement(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Unsolicited Neighbor Advertisements Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/arp/sent/success/unsolicited-neighbor-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdAsyncReceivedMiss(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Missed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/async/received/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdAsyncReceivedSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Received In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/async/received/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdAsyncSentArpFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Arp Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/async/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAsyncSentBufferAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Buffer Allocation Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/async/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdAsyncSentSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Sent In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/async/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdEchoReceived(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Received In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/echo/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdEchoSentArpFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Tx Arp Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/echo/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEchoSentBufferAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Tx Allocation Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/echo/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdEchoSentSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Sent In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/echo/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLinkDownLocalOper(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Local Oper Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/link-down/local-oper-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLinkDownRemoteAdmin(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Remote Admin Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/link-down/remote-admin-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdLinkDownRemote(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Remote Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/link-down/remote-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdLinkDownTimerExpiry(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Rx Timer Expiry</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/link-down/timer-expiry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdLinkUp(router, body, callback)</td>
    <td style="padding:15px">BFD Peer Path Link Up</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/link-up?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdLocalSourceNatChange(router, body, callback)</td>
    <td style="padding:15px">Peer Path Num Local Source Nat Change</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/local-source-nat-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdNeighborFailover(router, body, callback)</td>
    <td style="padding:15px">Number of failover events in neighbors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/neighbor/failover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdNeighborSourceNatChange(router, body, callback)</td>
    <td style="padding:15px">Number of source NAT changes in neighbors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/neighbor/source-nat-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerPathAsyncReceivedMiss(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Missed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/async/received/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerPathAsyncReceivedSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Received Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/async/received/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathAsyncSentArpFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Tx Arp Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/async/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAsyncSentBufferAllocationFailure2(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Tx Allocation Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/async/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerPathAsyncSentSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Async Packets Sent Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/async/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdPeerPathEchoReceived(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Received Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/echo/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathEchoSentArpFailure(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Arp Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/echo/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEchoSentBufferAllocationFailure2(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Buffer Allocation Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/echo/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerPathEchoSentSuccess(router, body, callback)</td>
    <td style="padding:15px">BFD Echo Packets Sent Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/echo/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdPeerPathJitter(router, body, callback)</td>
    <td style="padding:15px">Peer Path Jitter</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/jitter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdPeerPathLatency(router, body, callback)</td>
    <td style="padding:15px">Peer Path Latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLinkDownLocalOper2(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Local Oper Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/link-down/local-oper-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLinkDownRemoteAdmin2(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Remote Admin Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/link-down/remote-admin-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathLinkDownRemote(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Remote Down</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/link-down/remote-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathLinkDownTimerExpiry(router, body, callback)</td>
    <td style="padding:15px">Peer Path Link Down Triggered By Rx Timer Expiry</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/link-down/timer-expiry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdPeerPathLinkUp(router, body, callback)</td>
    <td style="padding:15px">BFD Peer Path Link Up</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/link-up?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathLocalSourceNatChange(router, body, callback)</td>
    <td style="padding:15px">Peer Path Num Local Source Nat Change</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/local-source-nat-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdPeerPathLoss(router, body, callback)</td>
    <td style="padding:15px">Peer Path Loss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/loss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdPeerPathMos(router, body, callback)</td>
    <td style="padding:15px">Peer Path MOS value</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/mos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdPeerPathNeighborFailover(router, body, callback)</td>
    <td style="padding:15px">Peer Path Num Neighbor Failover Event</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/neighbor/failover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathNeighborSourceNatChange(router, body, callback)</td>
    <td style="padding:15px">Peer Path Num Neighbor Source Nat Change</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/peer-path/neighbor/source-nat-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidAuthenticationField(router, body, callback)</td>
    <td style="padding:15px">BFD Invalid Auth Field</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/authentication-field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidDetectMultiple(router, body, callback)</td>
    <td style="padding:15px">BFD Invalid Multiplier</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/detect-multiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdReceivedInvalidDiscriminator(router, body, callback)</td>
    <td style="padding:15px">BFD Invalid Discriminator</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/discriminator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidHeaderLarge(router, body, callback)</td>
    <td style="padding:15px">BFD Packet Header Too Large</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/header-large?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidHeaderSmall(router, body, callback)</td>
    <td style="padding:15px">BFD Packet Header Too Small</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/header-small?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidHeaderVersion(router, body, callback)</td>
    <td style="padding:15px">Invalid BFD Header Version</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/header-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidLocalDiscriminator(router, body, callback)</td>
    <td style="padding:15px">Invalid BFD local discriminator</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/local-discriminator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdReceivedInvalidMetadata(router, body, callback)</td>
    <td style="padding:15px">Packets with invalid BFD metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidPacketLength(router, body, callback)</td>
    <td style="padding:15px">Invalid Length BFD Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/packet-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterBfdReceivedInvalidPayloadSmall(router, body, callback)</td>
    <td style="padding:15px">BFD Packets Too Small</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/payload-small?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsBfdReceivedInvalidSession(router, body, callback)</td>
    <td style="padding:15px">Invalid BFD session packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/bfd/received-invalid/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsConfigCacheHits(router, body, callback)</td>
    <td style="padding:15px">Count of cache hits</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/config/cache/hits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsConfigCacheMisses(router, body, callback)</td>
    <td style="padding:15px">Count of cache misses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/config/cache/misses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsConfluxBatchesLatency(router, body, callback)</td>
    <td style="padding:15px">Batch Latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/batches/latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxBatchesWriteDuration(router, body, callback)</td>
    <td style="padding:15px">Write Duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/batches/write/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxBatchesWriteFailure(router, body, callback)</td>
    <td style="padding:15px">Failed Batch Writes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/batches/write/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxBatchesWriteSuccess(router, body, callback)</td>
    <td style="padding:15px">Successful Batch Writes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/batches/write/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsConfluxGapsDetected(router, body, callback)</td>
    <td style="padding:15px">Gap Detections</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/gaps/detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxGapsResolutionRetry(router, body, callback)</td>
    <td style="padding:15px">Retried Gap Resolutions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/gaps/resolution/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxGapsResolutionSuccess(router, body, callback)</td>
    <td style="padding:15px">Successful Gap Resolutions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/gaps/resolution/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsConfluxMessagesRead(router, body, callback)</td>
    <td style="padding:15px">Message Reads</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/messages/read?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxMessagesWriteFailure(router, body, callback)</td>
    <td style="padding:15px">Failed Message Writes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/messages/write/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterConfluxMessagesWriteSuccess(router, body, callback)</td>
    <td style="padding:15px">Successful Message Writes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/conflux/messages/write/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsCpuUtilization(router, body, callback)</td>
    <td style="padding:15px">CPU utilization percentage</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/cpu/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDatabaseMemoryConsumed(router, body, callback)</td>
    <td style="padding:15px">Memory Consumed by the Database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/database/memory-consumed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDeviceInterfaceMessageFailure(router, body, callback)</td>
    <td style="padding:15px">Interface Config Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/device-interface/message-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDeviceInterfaceMessageSuccess(router, body, callback)</td>
    <td style="padding:15px">Interface Config Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/device-interface/message-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDiskCapacity(router, body, callback)</td>
    <td style="padding:15px">Total disk capacity</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/disk/capacity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDiskComponent(router, body, callback)</td>
    <td style="padding:15px">Component disk usage</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/disk/component?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsDiskUsed(router, body, callback)</td>
    <td style="padding:15px">Total disk used</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/disk/used?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiParsedFailureTls(router, body, callback)</td>
    <td style="padding:15px">TLS Parsing Failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/parsed/failure/tls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiParsedSuccessTls(router, body, callback)</td>
    <td style="padding:15px">TLS Certificate Parsed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/parsed/success/tls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiReceivedFailureCapacityExceeded(router, body, callback)</td>
    <td style="padding:15px">Capacity Exceeded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/received/failure/capacity-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedFailureTooManySegments(router, body, callback)</td>
    <td style="padding:15px">Too Many Segments</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/received/failure/too-many-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiReceivedSuccessAdd(router, body, callback)</td>
    <td style="padding:15px">Added to packet cache</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/received/success/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiReceivedSuccessDuplicate(router, body, callback)</td>
    <td style="padding:15px">Duplicate segments</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/received/success/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedSuccessReadyToParse(router, body, callback)</td>
    <td style="padding:15px">Ready to Parse</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dpi/received/success/ready-to-parse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateConnectionFirstConductorConnect(router, body, callback)</td>
    <td style="padding:15px">First conductor connect notifications</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/connection/first-conductor-connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateConnectionFirstServerConnect(router, body, callback)</td>
    <td style="padding:15px">First server connection notifications</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/connection/first-server-connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateConnectionLastConductorDisconnect(router, body, callback)</td>
    <td style="padding:15px">Last conductor disconnect notifications</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/connection/last-conductor-disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDynamicPeerUpdateLeadershipLost(router, body, callback)</td>
    <td style="padding:15px">Lost leadership notifications</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/leadership/lost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDynamicPeerUpdateLeadershipWon(router, body, callback)</td>
    <td style="padding:15px">Became leader notifications</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/leadership/won?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerUpdateRequestsReceivedPush(router, body, callback)</td>
    <td style="padding:15px">Push requests received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/requests/received/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerUpdateRequestsReceivedSync(router, body, callback)</td>
    <td style="padding:15px">Sync requests received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/requests/received/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRequestsReceivedSyncPeerAddresses(router, body, callback)</td>
    <td style="padding:15px">Sync peer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/requests/received/sync-peer-addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerUpdateRequestsSentPush(router, body, callback)</td>
    <td style="padding:15px">Push requests sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/requests/sent/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerUpdateRequestsSentSync(router, body, callback)</td>
    <td style="padding:15px">Sync requests sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/requests/sent/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateResponsesReceivedNotFound(router, body, callback)</td>
    <td style="padding:15px">Not found responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/responses/received/not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateResponsesReceivedOtherFailure(router, body, callback)</td>
    <td style="padding:15px">Other faulure responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/responses/received/other-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerUpdateResponsesReceivedSuccess(router, body, callback)</td>
    <td style="padding:15px">Success responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/responses/received/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateResponsesReceivedTimeOut(router, body, callback)</td>
    <td style="padding:15px">Timeout responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/dynamic-peer-update/responses/received/time-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentChannelMessagesReceived(router, body, callback)</td>
    <td style="padding:15px">Messages Received from the External Protocol's Channel</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/channel-messages/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentChannelMessagesSent(router, body, callback)</td>
    <td style="padding:15px">Messages Sent to the External Protocol's Channel</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/channel-messages/sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterErrorsBgpOverSvrDrops(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Bgp over SVR Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/bgp-over-svr-drops?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsClassifyDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Classify Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/classify-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsEarlyInbound(router, body, callback)</td>
    <td style="padding:15px">External Protocol Early Inbound Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/early-inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsEarlyOutbound(router, body, callback)</td>
    <td style="padding:15px">External Protocol Early Outbound Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/early-outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterErrorsFastlaneNotReadyDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Fastlane Not Ready Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/fastlane-not-ready-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterErrorsGlobalInterfaceLookupDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Global Interface Lookup Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/global-interface-lookup-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAgentErrorsIllegalEthernetDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Illegal Ethernet Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/illegal-ethernet-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIllegalExternalProtocolsChannelMessage(router, body, callback)</td>
    <td style="padding:15px">External Protocol Illegal Channel Message Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/illegal-external-protocols-channel-message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAgentErrorsIllegalIpDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Illegal IP Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/illegal-ip-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsInboundDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol Inbound Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/inbound-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInboundFromInternalInterfaceDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Inbound From Internal Interface Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/inbound-from-internal-interface-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsOutboundDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol Outbound Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/outbound-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterOutboundL2ResolutionReplyDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Outbound L2 Resolution Reply Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/outbound-l2-resolution-reply-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsAgentErrorsSessionCollision(router, body, callback)</td>
    <td style="padding:15px">External Protocol's Session Collision Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/errors/session-collision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterExternalProtocolsAgentPacketsInbound(router, body, callback)</td>
    <td style="padding:15px">Inbound External Protocol's Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/packets/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterExternalProtocolsAgentPacketsOutbound(router, body, callback)</td>
    <td style="padding:15px">Outbound External Protocol's Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/packets/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAgentPacketsSecureVectorRouted(router, body, callback)</td>
    <td style="padding:15px">Outbound External Protocol's Packets sent over a secure vector route</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/agent/packets/secure-vector-routed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerChannelMessagesReceived(router, body, callback)</td>
    <td style="padding:15px">Messages Received from the External Protocol's Channel</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/channel-messages/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerChannelMessagesSent(router, body, callback)</td>
    <td style="padding:15px">Messages Sent to the External Protocol's Channel</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/channel-messages/sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerErrorsEarlyInbound(router, body, callback)</td>
    <td style="padding:15px">External Protocol Early Inbound Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/errors/early-inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerErrorsEarlyOutbound(router, body, callback)</td>
    <td style="padding:15px">External Protocol Early Outbound Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/errors/early-outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIllegalExternalProtocolsChannelMessage2(router, body, callback)</td>
    <td style="padding:15px">External Protocol Illegal Channel Message Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/errors/illegal-external-protocols-channel-message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerErrorsInboundDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol Inbound Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/errors/inbound-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProtocolsManagerErrorsOutboundDrop(router, body, callback)</td>
    <td style="padding:15px">External Protocol Outbound Drop Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/errors/outbound-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterExternalProtocolsManagerPacketsInbound(router, body, callback)</td>
    <td style="padding:15px">Inbound External Protocol's Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/packets/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterExternalProtocolsManagerPacketsOutbound(router, body, callback)</td>
    <td style="padding:15px">Outbound External Protocol's Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/external-protocols/manager/packets/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayActionEventSendSuccess(router, body, callback)</td>
    <td style="padding:15px">Action Event messages sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/action-event/send-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayActionEventSendTimeout(router, body, callback)</td>
    <td style="padding:15px">Action Event messages timed out</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/action-event/send-timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsHighwayActionsActionMeter(router, body, callback)</td>
    <td style="padding:15px">Active Action Entries</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/actions/action-meter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityIcmpEstablished(router, body, callback)</td>
    <td style="padding:15px">ICMP sessions that were successfully established</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/icmp/established?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpTimeToEstablishmentMax(router, body, callback)</td>
    <td style="padding:15px">Maximum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/icmp/time-to-establishment/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpTimeToEstablishmentMin(router, body, callback)</td>
    <td style="padding:15px">Minimum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/icmp/time-to-establishment/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityIcmpTimeoutBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Timed out ICMP sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/icmp/timeout-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityIcmpUnreachable(router, body, callback)</td>
    <td style="padding:15px">ICMP unreachable</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/icmp/unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityTcpCloseBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Closed TCP sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/close-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityTcpEstablished(router, body, callback)</td>
    <td style="padding:15px">TCP sessions that were successfully established</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/established?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpTimeToEstablishmentMax(router, body, callback)</td>
    <td style="padding:15px">Maximum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/time-to-establishment/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpTimeToEstablishmentMin(router, body, callback)</td>
    <td style="padding:15px">Minimum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/time-to-establishment/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityTcpTimeoutBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Timed out TCP sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/timeout-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityTcpUnreachable(router, body, callback)</td>
    <td style="padding:15px">TCP unreachable</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tcp/unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityTlsCloseBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Closed TlS sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tls/close-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityTlsEstablished(router, body, callback)</td>
    <td style="padding:15px">TLS sessions that were successfully established</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tls/established?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTlsTimeToEstablishmentMax(router, body, callback)</td>
    <td style="padding:15px">Maximum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tls/time-to-establishment/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTlsTimeToEstablishmentMin(router, body, callback)</td>
    <td style="padding:15px">Minimum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tls/time-to-establishment/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityTlsTimeoutBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Timed out TLS sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/tls/timeout-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityUdpEstablished(router, body, callback)</td>
    <td style="padding:15px">UDP sessions that were successfully established</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/udp/established?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUdpTimeToEstablishmentMax(router, body, callback)</td>
    <td style="padding:15px">Maximum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/udp/time-to-establishment/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUdpTimeToEstablishmentMin(router, body, callback)</td>
    <td style="padding:15px">Minimum time to establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/udp/time-to-establishment/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReachabilityUdpTimeoutBeforeEstablishment(router, body, callback)</td>
    <td style="padding:15px">Timed out UDP sessions before establishment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/udp/timeout-before-establishment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayDestinationReachabilityUdpUnreachable(router, body, callback)</td>
    <td style="padding:15px">UDP unreachable</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/destination-reachability/udp/unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFirewallDetectorDiscoveryPacketDropped(router, body, callback)</td>
    <td style="padding:15px">Discovery packets dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/discovery-packet-dropped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorDiscoveryTimeout(router, body, callback)</td>
    <td style="padding:15px">Discovery timeout</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/discovery-timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFirewallDetectorDuplicateContextDetected(router, body, callback)</td>
    <td style="padding:15px">Duplicate context with missin peer name</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/duplicate-context-detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorReplyReceived(router, body, callback)</td>
    <td style="padding:15px">Discovery replies received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/reply-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorReplySent(router, body, callback)</td>
    <td style="padding:15px">Discovery replies sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/reply-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorRequestReceived(router, body, callback)</td>
    <td style="padding:15px">Discovery requests received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/request-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorRequestSent(router, body, callback)</td>
    <td style="padding:15px">Discovery requests sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/request-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFirewallDetectorTcpResetReceived(router, body, callback)</td>
    <td style="padding:15px">TCP Reset Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/tcp-reset-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHighwayFirewallDetectorUnknownReceived(router, body, callback)</td>
    <td style="padding:15px">Unknown Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/highway/firewall-detector/unknown-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpReceivedFailureDestinationUnreachable(router, body, callback)</td>
    <td style="padding:15px">ICMP Destination Unreachable Messages Dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/received/failure/destination-unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpReceivedSuccessDestinationUnreachable(router, body, callback)</td>
    <td style="padding:15px">ICMP Destination Unreachable Messages Processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/received/success/destination-unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDestinationUnreachablePacketTooBig(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable Packet Too Big</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/failure/destination-unreachable-packet-too-big?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpSentFailureEchoReply(router, body, callback)</td>
    <td style="padding:15px">ICMP Echo reply transmit failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/failure/echo-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentFailureEchoReplyDrop(router, body, callback)</td>
    <td style="padding:15px">ICMP Echo reply drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/failure/echo-reply-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDestinationUnreachableNet(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable Network Unreachable Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/destination-unreachable-net-unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDestinationUnreachableNotRequired(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable transmit not required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/destination-unreachable-not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDestinationUnreachablePacketTooBig2(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable Packet Too Big</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/destination-unreachable-packet-too-big?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDestinationUnreachableReassemblyTimeExceeded(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable Reassembly Time Exceeded Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/destination-unreachable-reassembly-time-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDestinationUnreachableTimeExceeded(router, body, callback)</td>
    <td style="padding:15px">ICMP Unreachable Time Exceeded Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/destination-unreachable-time-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpSentSuccessEchoReply(router, body, callback)</td>
    <td style="padding:15px">ICMP Echo reply transmit success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/icmp/sent/success/echo-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInterfaceReceivedBufferAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">Receive Buffer Allocation Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceReceivedBytes(router, body, callback)</td>
    <td style="padding:15px">Bytes Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/bytes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceReceivedError(router, body, callback)</td>
    <td style="padding:15px">Receive Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceReceivedMissed(router, body, callback)</td>
    <td style="padding:15px">Receive Missed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/missed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceReceivedPackets(router, body, callback)</td>
    <td style="padding:15px">Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceReceivedUtilization(router, body, callback)</td>
    <td style="padding:15px">Receive Utilization</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/received/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceSentBytes(router, body, callback)</td>
    <td style="padding:15px">Bytes Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/sent/bytes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceSentError(router, body, callback)</td>
    <td style="padding:15px">Send Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/sent/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceSentPackets(router, body, callback)</td>
    <td style="padding:15px">Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/sent/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsInterfaceSentUtilization(router, body, callback)</td>
    <td style="padding:15px">Transmit Utilization</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/interface/sent/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsIpfixRecordExportRate(router, body, callback)</td>
    <td style="padding:15px">Record Export Rate</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ipfix/record-export-rate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsIpfixTimePerExport(router, body, callback)</td>
    <td style="padding:15px">Generation Time per Export</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ipfix/time-per-export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsIpfixTimePerSession(router, body, callback)</td>
    <td style="padding:15px">Generation Time per Session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ipfix/time-per-session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsIpfixTotalGenerationTime(router, body, callback)</td>
    <td style="padding:15px">Total Generation Time</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ipfix/total-generation-time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsIpfixTotalRecords(router, body, callback)</td>
    <td style="padding:15px">Total Records Exported</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ipfix/total-records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMemoryCapacity(router, body, callback)</td>
    <td style="padding:15px">Total memory capacity</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/memory/capacity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMemoryUsed(router, body, callback)</td>
    <td style="padding:15px">Total memory used</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/memory/used?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsActive(router, body, callback)</td>
    <td style="padding:15px">Number of Active Metrics</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/active-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsCollectionsPending(router, body, callback)</td>
    <td style="padding:15px">Number of Pending Aggregations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/collections-pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsCollectionsSkipped(router, body, callback)</td>
    <td style="padding:15px">Number of Metric Collection Skips</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/collections-skipped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsPerCollectionLatency(router, body, callback)</td>
    <td style="padding:15px">Latency per Collection</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/per-collection-latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsPerMetricLatency(router, body, callback)</td>
    <td style="padding:15px">Amortized per-metric collection latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/per-metric-latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMetricsReadMetricLatency(router, body, callback)</td>
    <td style="padding:15px">Amortized per-metric read latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/metrics/read-metric-latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMtuDiscoveryPeerPathReceived(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Received Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/peer-path/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPeerPathSentArpFailure(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Arp Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/peer-path/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathSentBufferAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Buffer Allocation Failure Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/peer-path/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDiscoveryPeerPathSentSuccess(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Sent Per Peer Path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/peer-path/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMtuDiscoveryReceived(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Received In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMtuDiscoverySentArpFailure(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Tx Arp Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/sent/arp-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDiscoverySentBufferAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Tx Allocation Failure In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/sent/buffer-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsMtuDiscoverySentSuccess(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery Packets Sent In Total</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/mtu-discovery/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsPacketCaptureSuccess(router, body, callback)</td>
    <td style="padding:15px">Packets send to be captured</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-capture/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsPacketCaptureWriteFailure(router, body, callback)</td>
    <td style="padding:15px">Packet capture write failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-capture/write-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesDataNormalization(router, body, callback)</td>
    <td style="padding:15px">Aes Data Normalization Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/data-normalization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesDecryptionException(router, body, callback)</td>
    <td style="padding:15px">AES Decryption Exceptions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/decryption-exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesEncryptionException(router, body, callback)</td>
    <td style="padding:15px">AES Encryption Exceptions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/encryption-exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureAesGetDataLength(router, body, callback)</td>
    <td style="padding:15px">AES Get Data Length Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/get-data-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesIvAppend(router, body, callback)</td>
    <td style="padding:15px">AES IV Append Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/iv-append?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesIvGeneration(router, body, callback)</td>
    <td style="padding:15px">AES IV Generation Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/iv-generation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesIvSeed(router, body, callback)</td>
    <td style="padding:15px">AES IV Seed Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/iv-seed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesMetadataDecryption(router, body, callback)</td>
    <td style="padding:15px">AES Metadata Decryption Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/metadata-decryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesMetadataEncryption(router, body, callback)</td>
    <td style="padding:15px">AES Metadata Encryption Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/metadata-encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureAesNoContextFound(router, body, callback)</td>
    <td style="padding:15px">AES No Context Found</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/no-context-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesPayloadDecryption(router, body, callback)</td>
    <td style="padding:15px">AES Payload Decryption Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/payload-decryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureAesPayloadEncryption(router, body, callback)</td>
    <td style="padding:15px">AES Payload Encryption Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/payload-encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureAesSetDataLength(router, body, callback)</td>
    <td style="padding:15px">AES Set Data Length Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/aes/set-data-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdDecode(router, body, callback)</td>
    <td style="padding:15px">Failure to decode bfd packet</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/decode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdEchoInit(router, body, callback)</td>
    <td style="padding:15px">Echo action initialization failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/echo-init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdEmptyMetadata(router, body, callback)</td>
    <td style="padding:15px">Failure due to metadata being absent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/empty-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdEmptyPayload(router, body, callback)</td>
    <td style="padding:15px">Failure due to empty payload</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/empty-payload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureBfdFragmentedMtuDiscovery(router, body, callback)</td>
    <td style="padding:15px">Received fragmented MTU discovery packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/fragmented-mtu-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdInvalidHeader(router, body, callback)</td>
    <td style="padding:15px">Failure due to invalid bfd header</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/invalid-header?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureBfdMetadataParse(router, body, callback)</td>
    <td style="padding:15px">Failure to parse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/bfd/metadata-parse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureDpiFtpParseFailed(router, body, callback)</td>
    <td style="padding:15px">FTP Parse Failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/dpi/ftp/parse-failed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureDpiFtpPinholeMiss(router, body, callback)</td>
    <td style="padding:15px">FTP Data Port Pinhole Miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/dpi/ftp/pinhole-miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureDpiFtpPinholeTimeout(router, body, callback)</td>
    <td style="padding:15px">FTP Pinhole Timeouts</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/dpi/ftp/pinhole-timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHeaderTransformArpTableMisses(router, body, callback)</td>
    <td style="padding:15px">Arp Table Misses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ethernet-header-transform/arp-table-misses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEthernetHeaderTransformPacketExpansion(router, body, callback)</td>
    <td style="padding:15px">Packet Expansion Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ethernet-header-transform/packet-expansion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMoveDisableForwardMetadataDiscards(router, body, callback)</td>
    <td style="padding:15px">Disable Forward Metadata Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/flow-move/disable-forward-metadata-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureHmacAllocation(router, body, callback)</td>
    <td style="padding:15px">HMAC Allocation Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureHmacCannotFindDigest(router, body, callback)</td>
    <td style="padding:15px">HMAC Cannot Find Digest</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/cannot-find-digest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureHmacContextNotFound(router, body, callback)</td>
    <td style="padding:15px">HMAC Context Not Found</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/context-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureHmacDigestCalculation(router, body, callback)</td>
    <td style="padding:15px">HMAC Digest Calculation Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/digest-calculation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureHmacDigestCompare(router, body, callback)</td>
    <td style="padding:15px">HMAC Digest Compare Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/digest-compare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterHmacOppositeFlowNotFound(router, body, callback)</td>
    <td style="padding:15px">Opposite Flow Not Found</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/hmac/opposite-flow-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIpHeaderTransformIcmpTranslation(router, body, callback)</td>
    <td style="padding:15px">Failure in ICMP header translation</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ip-header-transform/icmp-translation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTransformIpv6ToIpv4Mapping(router, body, callback)</td>
    <td style="padding:15px">Failure in IPv6 to IPv4 address mapping</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ip-header-transform/ipv6-to-ipv4-mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIpHeaderTransformL4Translation(router, body, callback)</td>
    <td style="padding:15px">Failure in L4 header translation</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ip-header-transform/l4-translation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureKeepAliveBufferAllocation(router, body, callback)</td>
    <td style="padding:15px">Keep-alive buffer allocation failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/keep-alive/buffer-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureMetadataAdd(router, body, callback)</td>
    <td style="padding:15px">Metadata Add Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataCachedAttributesNotFound(router, body, callback)</td>
    <td style="padding:15px">Metadata Propagate Cached Attributes Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/cached-attributes-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataEnableFlowMiss(router, body, callback)</td>
    <td style="padding:15px">Received enable-metadata packet with no matching flow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/enable-metadata-flow-miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureMetadataGetLength(router, body, callback)</td>
    <td style="padding:15px">Metadata Get Length Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/get-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureMetadataInvalidType(router, body, callback)</td>
    <td style="padding:15px">Invalid metadata type received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/invalid-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureMetadataNotPresent(router, body, callback)</td>
    <td style="padding:15px">Metadata Not Present when required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/not-present?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedUnknownForcedDropReason(router, body, callback)</td>
    <td style="padding:15px">Unknown Forced Drop Reason</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/received-unknown-forced-drop-reason?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureMetadataReverseBufferAllocation(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packet buffer allocation failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/reverse-buffer-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataTurnOffBufferAllocation(router, body, callback)</td>
    <td style="padding:15px">Metadata turn-off packet buffer allocation failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/metadata/turn-off-buffer-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureMultiplexorUntagDiscards(router, body, callback)</td>
    <td style="padding:15px">Untag Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/multiplexor/untag-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailurePaddingInvalidPacketLength(router, body, callback)</td>
    <td style="padding:15px">Invalid Length</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/padding/invalid-packet-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsSentCorrelatedData(router, body, callback)</td>
    <td style="padding:15px">Failed to Send Correlated Data</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/path-metrics/sent-correlated-data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsSentUnidirectionalReply(router, body, callback)</td>
    <td style="padding:15px">Failed to Send Unidirectional Reply Packet</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/path-metrics/sent-unidirectional-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureRateLimitCheck(router, body, callback)</td>
    <td style="padding:15px">Rate Limit Drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/rate-limit-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureRetransmissionHandlerBufferAllocation(router, body, callback)</td>
    <td style="padding:15px">Retransmission packet buffer allocation failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/buffer-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureRetransmissionHandlerCapacityExceeded(router, body, callback)</td>
    <td style="padding:15px">Capacity Exceeded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/capacity-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRetransmissionHandlerDeletedFlowDiscards(router, body, callback)</td>
    <td style="padding:15px">Packets discarded when flow is invalidated</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/deleted-flow-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRetransmissionHandlerInvalidAcknowledgementAttribute(router, body, callback)</td>
    <td style="padding:15px">Invalid Acknowledgement Attribute</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/invalid-acknowledgement-attribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRetransmissionHandlerInvalidPacketLength(router, body, callback)</td>
    <td style="padding:15px">Invalid Packet Length</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/invalid-packet-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionFailureRetransmissionHandlerReclassify(router, body, callback)</td>
    <td style="padding:15px">Reclassification Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/reclassify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureRetransmissionHandlerTimeoutDiscards(router, body, callback)</td>
    <td style="padding:15px">Packets Discarded Due to Timeout</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/retransmission-handler/timeout-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureTcpProxyBufferAllocation(router, body, callback)</td>
    <td style="padding:15px">TCP Proxy buffer allocation failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/tcp-proxy/buffer-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFailureTcpProxyInboundDiscards(router, body, callback)</td>
    <td style="padding:15px">Inbound discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/tcp-proxy/inbound-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpProxySessionsTimedOut(router, body, callback)</td>
    <td style="padding:15px">Sessions closed due to the data timing out</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/tcp-proxy/sessions-timed-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpStateIllegalFlagCombination(router, body, callback)</td>
    <td style="padding:15px">TCP State Illegal Flag Combinations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/tcp-state/illegal-flag-combination?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpStateInvalidTransition(router, body, callback)</td>
    <td style="padding:15px">TCP State Invalid Transitions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/tcp-state/invalid-state-transition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingActionFailureTtlValidate(router, body, callback)</td>
    <td style="padding:15px">TTL Validate Drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/failure/ttl-validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesMetadataDecryption(router, body, callback)</td>
    <td style="padding:15px">AES Metadata Decryption Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/metadata-decryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesMetadataEncryption(router, body, callback)</td>
    <td style="padding:15px">AES Metadata Encryption Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/metadata-encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAesMetadataFalsePositiveDetected(router, body, callback)</td>
    <td style="padding:15px">Metadata False Positive Detected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/metadata-false-positive-detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesNoMetadata(router, body, callback)</td>
    <td style="padding:15px">No AES Processing Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/no-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesNoPayload(router, body, callback)</td>
    <td style="padding:15px">No AES Processing Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/no-payload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesPayloadDecryption(router, body, callback)</td>
    <td style="padding:15px">AES Payload Decryption Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/payload-decryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessAesPayloadEncryption(router, body, callback)</td>
    <td style="padding:15px">AES Payload Encryption Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/aes/payload-encryption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessBfdAsyncDiscarded(router, body, callback)</td>
    <td style="padding:15px">Async packet discarded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/async-discarded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessBfdAsyncProcessed(router, body, callback)</td>
    <td style="padding:15px">Async packet processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/async-processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessBfdEchoRequestProcessed(router, body, callback)</td>
    <td style="padding:15px">Echo request packet processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/echo-request-processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessBfdEchoResponseProcessed(router, body, callback)</td>
    <td style="padding:15px">Echo response packet processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/echo-response-processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessBfdMtuDiscoveryRequests(router, body, callback)</td>
    <td style="padding:15px">MTU Discovery request packets received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/mtu-discovery-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessBfdRemoteSessionRequested(router, body, callback)</td>
    <td style="padding:15px">Remote sessions requested</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/bfd/remote-session-requested?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingActionSuccessDetour(router, body, callback)</td>
    <td style="padding:15px">Detour Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/detour?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiFtpDataPortDetected(router, body, callback)</td>
    <td style="padding:15px">FTP Data Port Detected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/ftp/data-port-detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDpiFtpPinholeClosed(router, body, callback)</td>
    <td style="padding:15px">FTP Pinholes Closed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/ftp/pinhole-closed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDpiFtpPinholeHit(router, body, callback)</td>
    <td style="padding:15px">FTP Data Port Pinhole Hit</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/ftp/pinhole-hit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessDpiFtpPinholeOpened(router, body, callback)</td>
    <td style="padding:15px">FTP Pinholes Opened</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/ftp/pinhole-opened?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiTlsClientDetected(router, body, callback)</td>
    <td style="padding:15px">Encrypted Session Detected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/tls/client/detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDpiTlsServerHandshakeDetected(router, body, callback)</td>
    <td style="padding:15px">Encrypted Session Detected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/tls/server/handshake-detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTlsServerSentForInspection(router, body, callback)</td>
    <td style="padding:15px">More Inspection Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/tls/server/sent-for-inspection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessDpiUnencrypted(router, body, callback)</td>
    <td style="padding:15px">Dynamic Encryption Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/dpi/unencrypted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingActionSuccessDrop(router, body, callback)</td>
    <td style="padding:15px">Drop Action Matches</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessDuplicateDetectorAllow(router, body, callback)</td>
    <td style="padding:15px">Packets Allowed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/duplicate-detector/allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessDuplicateDetectorDeny(router, body, callback)</td>
    <td style="padding:15px">Packets Denied</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/duplicate-detector/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessEthernetHeaderTransform(router, body, callback)</td>
    <td style="padding:15px">Encapsulated in Ethernet Header</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/ethernet-header-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessFlowMoveDetected(router, body, callback)</td>
    <td style="padding:15px">Flow Move Detected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/flow-move/detected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessFlowMovePacketsEnqueued(router, body, callback)</td>
    <td style="padding:15px">Packets Enqueued for Flow Move</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/flow-move/packets-enqueued?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessForwardToWire(router, body, callback)</td>
    <td style="padding:15px">Packets Forwarded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/forward/to-wire?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessHmacAppendNoPayload(router, body, callback)</td>
    <td style="padding:15px">No Payload for HMAC Digest</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/hmac/append-no-payload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessHmacDigestAdded(router, body, callback)</td>
    <td style="padding:15px">Digest Added</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/hmac/digest-added?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessHmacDigestRemoved(router, body, callback)</td>
    <td style="padding:15px">Digest Removed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/hmac/digest-removed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessHmacNotRequired(router, body, callback)</td>
    <td style="padding:15px">Digest Not Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/hmac/not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessHmacValidateNoPayload(router, body, callback)</td>
    <td style="padding:15px">No HMAC Digest Available</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/hmac/validate-no-payload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessIpHeaderTransform(router, body, callback)</td>
    <td style="padding:15px">transform success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/ip-header-transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessKeepAliveSentTcp(router, body, callback)</td>
    <td style="padding:15px">TCP NAT Keep Alive Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/keep-alive/sent-tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessKeepAliveSentUdp(router, body, callback)</td>
    <td style="padding:15px">UDP NAT Keep Alive Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/keep-alive/sent-udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessKeepAliveSessionsClosed(router, body, callback)</td>
    <td style="padding:15px">Sessions closed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/keep-alive/sessions-closed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessKeepAliveSessionsOpened(router, body, callback)</td>
    <td style="padding:15px">Sessions opened</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/keep-alive/sessions-opened?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataAddFalsePositiveCorrection(router, body, callback)</td>
    <td style="padding:15px">Metadata Added for False Positive Detection</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/add-false-positive-correction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataAddNotRequired(router, body, callback)</td>
    <td style="padding:15px">Metadata Add Not Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/add-not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessMetadataAdded(router, body, callback)</td>
    <td style="padding:15px">Metadata Added</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/added?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataPropagateNotRequired(router, body, callback)</td>
    <td style="padding:15px">Metadata Propagate Not Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/propagate-not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataReceivedDisableForward(router, body, callback)</td>
    <td style="padding:15px">Received Disable Forward Metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/received-disable-forward-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataReceivedEnable(router, body, callback)</td>
    <td style="padding:15px">Received Enable Metadata Packet</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/received-enable-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataReceivedKeepAlive(router, body, callback)</td>
    <td style="padding:15px">Received Keep-Alive Packet</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/received-keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataRemoveFalsePositiveCorrection(router, body, callback)</td>
    <td style="padding:15px">Metadata Removed after False Positive Detection</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/remove-false-positive-correction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataRemoveNotRequired(router, body, callback)</td>
    <td style="padding:15px">Metadata Remove Not Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/remove-not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessMetadataRemoved(router, body, callback)</td>
    <td style="padding:15px">Metadata Removed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/removed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessMetadataRightLaneSent(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packets that were succcessfully sent to right lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/right-lane-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterMetadataSentDisableForward(router, body, callback)</td>
    <td style="padding:15px">Sent Disable Forward Metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/sent-disable-forward-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessMetadataUpdated(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata in packet updated successfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/metadata/updated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessMultiplexorTagged(router, body, callback)</td>
    <td style="padding:15px">Tag Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/multiplexor/tagged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessMultiplexorUntagged(router, body, callback)</td>
    <td style="padding:15px">Untag Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/multiplexor/untagged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessPaddingAdded(router, body, callback)</td>
    <td style="padding:15px">Padding Added</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/padding/added?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessPaddingRemoved(router, body, callback)</td>
    <td style="padding:15px">Padding Removed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/padding/removed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsAddNotRequired(router, body, callback)</td>
    <td style="padding:15px">Path Metrics Add Not Required</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/add-not-required?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessPathMetricsRemoveDropped(router, body, callback)</td>
    <td style="padding:15px">Remove Path Metrics Dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/remove-dropped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsRemoveNotPresent(router, body, callback)</td>
    <td style="padding:15px">Path Metrics Not Present</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/remove-not-present?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessPathMetricsRemoved(router, body, callback)</td>
    <td style="padding:15px">Path Metrics Removed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/removed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsSentCorrelatedData2(router, body, callback)</td>
    <td style="padding:15px">Sent Correlated Data</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/sent-correlated-data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsSentUnidirectionalReply2(router, body, callback)</td>
    <td style="padding:15px">Sent Unidrectional Reply Packet</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/sent-unidirectional-reply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsWithMetadataAdded(router, body, callback)</td>
    <td style="padding:15px">Path Metrics Added to Packets with Metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/with-metadata-added?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPathMetricsWithoutMetadataAdded(router, body, callback)</td>
    <td style="padding:15px">Path Metrics Added to Packets without Metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/path-metrics/without-metadata-added?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessRateLimitCheck(router, body, callback)</td>
    <td style="padding:15px">Rate Limit Valid</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/rate-limit-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessRetransmissionHandlerAccepted(router, body, callback)</td>
    <td style="padding:15px">Packets Accepted</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/accepted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessRetransmissionHandlerAcknowledged(router, body, callback)</td>
    <td style="padding:15px">Packets Acknowledged</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/acknowledged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerAcknowledgementReceived(router, body, callback)</td>
    <td style="padding:15px">Acknowledgement Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/acknowledgement-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerAcknowledgementSent(router, body, callback)</td>
    <td style="padding:15px">Acknowledgement Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/acknowledgement-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessRetransmissionHandlerBypassed(router, body, callback)</td>
    <td style="padding:15px">Packets Bypassed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/bypassed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterActionSuccessRetransmissionHandlerCached(router, body, callback)</td>
    <td style="padding:15px">Packets Cached</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/cached?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerDuplicateDiscards(router, body, callback)</td>
    <td style="padding:15px">Packets Discarded Due to Retransmissions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/duplicate-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRetransmissionHandlerOutstandingPackets(router, body, callback)</td>
    <td style="padding:15px">Outstanding retransmission packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/outstanding-retransmission-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerRetransmissionsSent(router, body, callback)</td>
    <td style="padding:15px">Retransmissions sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/retransmissions-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerSessionsClosed(router, body, callback)</td>
    <td style="padding:15px">Sessions closed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/sessions-closed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessRetransmissionHandlerSessionsOpened(router, body, callback)</td>
    <td style="padding:15px">Sessions opened</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/retransmission-handler/sessions-opened?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxyAcknowledgementReceived(router, body, callback)</td>
    <td style="padding:15px">Acknowledgements received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/acknowledgement-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxyAcknowledgementSent(router, body, callback)</td>
    <td style="padding:15px">Acknowledgements sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/acknowledgement-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpProxyDataPacketsTransmitted(router, body, callback)</td>
    <td style="padding:15px">Data packets transmitted</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/data-packets-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxyDataTransmitted(router, body, callback)</td>
    <td style="padding:15px">Data transmitted</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/data-transmitted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpProxyOutstandingPackets(router, body, callback)</td>
    <td style="padding:15px">Outstanding proxy packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/outstanding-proxy-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxyRetransmissionsSent(router, body, callback)</td>
    <td style="padding:15px">Retransmissions sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/retransmissions-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTcpProxyRetransmittedDataSent(router, body, callback)</td>
    <td style="padding:15px">Retransmitted data sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/retransmitted-data-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxySessionsClosed(router, body, callback)</td>
    <td style="padding:15px">Sessions closed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/sessions-closed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxySessionsOpened(router, body, callback)</td>
    <td style="padding:15px">Sessions opened</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/sessions-opened?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSuccessTcpProxySessionsReset(router, body, callback)</td>
    <td style="padding:15px">Sessions closed due to receiving a reset</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-proxy/sessions-reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingActionSuccessTcpState(router, body, callback)</td>
    <td style="padding:15px">Tcp State Valid</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/tcp-state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingActionSuccessTtlValidate(router, body, callback)</td>
    <td style="padding:15px">Ttl Valid</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/action/success/ttl-validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedArp(router, body, callback)</td>
    <td style="padding:15px">ARP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedBroadcastMacDiscards(router, body, callback)</td>
    <td style="padding:15px">Broadcast Packet Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/broadcast-mac-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedDhcp(router, body, callback)</td>
    <td style="padding:15px">DHCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedDhcpv6(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/dhcpv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedDiscards(router, body, callback)</td>
    <td style="padding:15px">Total Classification Drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedException(router, body, callback)</td>
    <td style="padding:15px">Classifier Exceptions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedFragmentedIcmpDiscards(router, body, callback)</td>
    <td style="padding:15px">Fragmented ICMP Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/fragmented-icmp-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedFragmentedIcmpv6Discards(router, body, callback)</td>
    <td style="padding:15px">Fragmented ICMPv6 Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/fragmented-icmpv6-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedIcmp(router, body, callback)</td>
    <td style="padding:15px">ICMP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIcmpTtlDiscards(router, body, callback)</td>
    <td style="padding:15px">ICMP TTL Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmp-ttl-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedIcmpUnknownTypeDiscards(router, body, callback)</td>
    <td style="padding:15px">Unknown ICMP Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmp-unknown-type-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedIcmpv6(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmpv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedIcmpv6HopLimitDiscards(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Hop Limit Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmpv6-hop-limit-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedIcmpv6LinkLocalDiscards(router, body, callback)</td>
    <td style="padding:15px">Link Local ICMPv6 Packet Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmpv6-link-local-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedIcmpv6UnknownTypeDiscards(router, body, callback)</td>
    <td style="padding:15px">Unknown ICMPv6 Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/icmpv6-unknown-type-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpInvalid(router, body, callback)</td>
    <td style="padding:15px">IGMP Packets discarded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/invalid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpLeave(router, body, callback)</td>
    <td style="padding:15px">IGMP leave</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/leave?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpQuery(router, body, callback)</td>
    <td style="padding:15px">IGMP query</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpReport(router, body, callback)</td>
    <td style="padding:15px">IGMP report</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpReportV3(router, body, callback)</td>
    <td style="padding:15px">IGMP report v3</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/report-v3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIgmpTotal(router, body, callback)</td>
    <td style="padding:15px">Total IGMP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/igmp/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedInvalidHeaderLength(router, body, callback)</td>
    <td style="padding:15px">Invalid Header Length Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/invalid-header-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedInvalidIpv6(router, body, callback)</td>
    <td style="padding:15px">Invalid IPv6 Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/invalid-ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedInvalidLength(router, body, callback)</td>
    <td style="padding:15px">Invalid Length Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/invalid-length?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedIpv4(router, body, callback)</td>
    <td style="padding:15px">IPv4 Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv4?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIpv4FabricFragmented(router, body, callback)</td>
    <td style="padding:15px">IPv4 Fabric Fragment Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv4-fabric-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedIpv4Fragmented(router, body, callback)</td>
    <td style="padding:15px">IPv4 Fragment Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv4-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedIpv6(router, body, callback)</td>
    <td style="padding:15px">IPv6 Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedIpv6FabricFragmented(router, body, callback)</td>
    <td style="padding:15px">IPv6 Fabric Fragment Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv6-fabric-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedIpv6Fragmented(router, body, callback)</td>
    <td style="padding:15px">IPv6 Fragment Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ipv6-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedIs(router, body, callback)</td>
    <td style="padding:15px">IS-IS Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/is-is?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedLacp(router, body, callback)</td>
    <td style="padding:15px">LACP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/lacp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedNdp(router, body, callback)</td>
    <td style="padding:15px">NDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ndp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedOspf(router, body, callback)</td>
    <td style="padding:15px">OSPF Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedOtherLayer4Protocol(router, body, callback)</td>
    <td style="padding:15px">Other Layer 4 Protocol Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/other-layer-4-protocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierReceivedAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Router Advertisement Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/router-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedSctp(router, body, callback)</td>
    <td style="padding:15px">SCTP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/sctp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierReceivedSpanningTreeDiscards(router, body, callback)</td>
    <td style="padding:15px">Spanning Tree Discards</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/spanning-tree-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedTotal(router, body, callback)</td>
    <td style="padding:15px">Total Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedTunnel(router, body, callback)</td>
    <td style="padding:15px">Tunneled Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedUnknownLayer3ProtocolDiscards(router, body, callback)</td>
    <td style="padding:15px">Received Unclassified Packets Dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/unknown-layer-3-protocol-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedVlan(router, body, callback)</td>
    <td style="padding:15px">VLAN Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierReceivedVrrp(router, body, callback)</td>
    <td style="padding:15px">VRRP Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/received/vrrp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentArp(router, body, callback)</td>
    <td style="padding:15px">ARP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentDhcp(router, body, callback)</td>
    <td style="padding:15px">DHCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentDhcpv6(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/dhcpv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentIcmp(router, body, callback)</td>
    <td style="padding:15px">ICMP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/icmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentIcmpv6(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/icmpv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentIgmp(router, body, callback)</td>
    <td style="padding:15px">IGMP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/igmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentIpv4(router, body, callback)</td>
    <td style="padding:15px">IPv4 Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv4?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierSentIpv4FabricFragmented(router, body, callback)</td>
    <td style="padding:15px">IPv4 Fabric Fragment Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv4-fabric-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierSentIpv4Fragmented(router, body, callback)</td>
    <td style="padding:15px">IPv4 Fragment Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv4-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentIpv6(router, body, callback)</td>
    <td style="padding:15px">IPv6 Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierSentIpv6FabricFragmented(router, body, callback)</td>
    <td style="padding:15px">IPv6 Fabric Fragment Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv6-fabric-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierSentIpv6Fragmented(router, body, callback)</td>
    <td style="padding:15px">IPv4 Fragment Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ipv6-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierSentIs(router, body, callback)</td>
    <td style="padding:15px">IS-IS Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/is-is?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentLacp(router, body, callback)</td>
    <td style="padding:15px">LACP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/lacp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentNdp(router, body, callback)</td>
    <td style="padding:15px">NDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ndp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentOspf(router, body, callback)</td>
    <td style="padding:15px">OSPF Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierSentOtherLayer3Protocol(router, body, callback)</td>
    <td style="padding:15px">Other Layer 3 Protocol Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/other-layer-3-protocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClassifierSentOtherLayer4Protocol(router, body, callback)</td>
    <td style="padding:15px">Other Layer 4 Protocol Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/other-layer-4-protocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingClassifierSentAdvertisement(router, body, callback)</td>
    <td style="padding:15px">Router Advertisement Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/router-advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentSctp(router, body, callback)</td>
    <td style="padding:15px">SCTP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/sctp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentTcp(router, body, callback)</td>
    <td style="padding:15px">TCP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/tcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentTotal(router, body, callback)</td>
    <td style="padding:15px">Total Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentTunnel(router, body, callback)</td>
    <td style="padding:15px">Tunneled Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentUdp(router, body, callback)</td>
    <td style="padding:15px">UDP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/udp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentVlan(router, body, callback)</td>
    <td style="padding:15px">VLAN Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingClassifierSentVrrp(router, body, callback)</td>
    <td style="padding:15px">VRRP Packets Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/classifier/sent/vrrp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsPacketProcessingDhcpDiscarded(router, body, callback)</td>
    <td style="padding:15px">Discarded Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/dhcp/discarded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpForwardToDerivedInterface(router, body, callback)</td>
    <td style="padding:15px">Packets Forwarded to Derived Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/dhcp/forward-to-derived-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpForwardToOriginatingInterface(router, body, callback)</td>
    <td style="padding:15px">Packets Forwarded to Originating Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/dhcp/forward-to-originating-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpReceivedForInternalApplication(router, body, callback)</td>
    <td style="padding:15px">Packets Received for Internal DHCP Application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/dhcp/received-for-internal-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEnqueueToDeferredRingFailure(router, body, callback)</td>
    <td style="padding:15px">Deferred Ring Overflow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/enqueue/to-deferred-ring-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEnqueueToDeferredRingSuccess(router, body, callback)</td>
    <td style="padding:15px">Deferred Ring Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/enqueue/to-deferred-ring-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEnqueueToWorkerCoreFailure(router, body, callback)</td>
    <td style="padding:15px">Failures Re-Enqueuing Fast Lane Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/enqueue/to-worker-core-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEnqueueToWorkerCoreSuccess(router, body, callback)</td>
    <td style="padding:15px">Re-Enqueued Fast Lane Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/enqueue/to-worker-core-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFibActionSummaryException(router, body, callback)</td>
    <td style="padding:15px">Exception in Fib Packet Action Processing</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fib-action-summary/exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFibActionSummaryFailure(router, body, callback)</td>
    <td style="padding:15px">Fib Packet Action Processing Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fib-action-summary/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFibActionSummarySuccess(router, body, callback)</td>
    <td style="padding:15px">Success Fib Packet Action Processing</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fib-action-summary/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFlowActionSummaryException(router, body, callback)</td>
    <td style="padding:15px">Exception in Flow Packet Action Processing</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/flow-action-summary/exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFlowActionSummaryFailure(router, body, callback)</td>
    <td style="padding:15px">Flow Packet Action Processing Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/flow-action-summary/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFlowActionSummarySuccess(router, body, callback)</td>
    <td style="padding:15px">Success Flow Packet Action Processing</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/flow-action-summary/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedDuplicateFirstFragment(router, body, callback)</td>
    <td style="padding:15px">Duplicate First Fragment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/duplicate-first-fragment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedDuplicateLastFragment(router, body, callback)</td>
    <td style="padding:15px">Duplicate Last Fragment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/duplicate-last-fragment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedFailureToReassemble(router, body, callback)</td>
    <td style="padding:15px">Failure to Reassemble</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/failure-to-reassemble?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedFragmentChainsDiscarded(router, body, callback)</td>
    <td style="padding:15px">Discarded Fragment Chains</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/fragment-chains-discarded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedFragmentChainsExceeded(router, body, callback)</td>
    <td style="padding:15px">Exceeded Fragment Chains</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/fragment-chains-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationReceivedFragmentChainsTimeout(router, body, callback)</td>
    <td style="padding:15px">Timeout Fragment Chains</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/fragment-chains-timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFragmentationReceivedIncompleteFragments(router, body, callback)</td>
    <td style="padding:15px">Incomplete Fragments</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/incomplete-fragments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedInvalidLengthFirstFragment(router, body, callback)</td>
    <td style="padding:15px">Invalid Length First Fragment</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/invalid-length-first-fragment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingFragmentationReceivedSuccessfullyReassembled(router, body, callback)</td>
    <td style="padding:15px">Successfully Reassembled</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/received/successfully-reassembled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentFragmentCreationFailure(router, body, callback)</td>
    <td style="padding:15px">Fragment Creation Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/fragment-creation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentIpv4DontFragmentDrop(router, body, callback)</td>
    <td style="padding:15px">Jumbo IPv4 Don't Fragment Packets Dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv4-dont-fragment-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentIpv4FabricFragments(router, body, callback)</td>
    <td style="padding:15px">Fabric IPv4 Fragmented Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv4-fabric-fragments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentIpv4NonFabricFragments(router, body, callback)</td>
    <td style="padding:15px">Non-Fabric IPv6 Fragmented Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv4-non-fabric-fragments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentIpv4PacketsFragmented(router, body, callback)</td>
    <td style="padding:15px">Fragmented IPv4 Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv4-packets-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentIpv6FabricFragments(router, body, callback)</td>
    <td style="padding:15px">Fabric IPv6 Fragmented Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv6-fabric-fragments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentIpv6MtuExceededDrop(router, body, callback)</td>
    <td style="padding:15px">IPv6 MTU Exceeded Packets Dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv6-mtu-exceeded-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentIpv6NonFabricFragments(router, body, callback)</td>
    <td style="padding:15px">Non-Fabric IPv6 Fragmented Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv6-non-fabric-fragments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentIpv6PacketsFragmented(router, body, callback)</td>
    <td style="padding:15px">Fragmented IPv6 Packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/ipv6-packets-fragmented?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFragmentationSentUnknownL2Version(router, body, callback)</td>
    <td style="padding:15px">Unknown L2 Version</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/fragmentation/sent/unknown-l2-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupAccessPolicyTableAllow(router, body, callback)</td>
    <td style="padding:15px">Access Policy Table Allow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/access-policy-table/allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupAccessPolicyTableDeny(router, body, callback)</td>
    <td style="padding:15px">Access Policy Table Deny</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/access-policy-table/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupAccessPolicyTableFailure(router, body, callback)</td>
    <td style="padding:15px">Failure Locking access-policy table</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/access-policy-table/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupAccessPolicyTableMiss(router, body, callback)</td>
    <td style="padding:15px">Access Policy Table Miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/access-policy-table/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFibTableDeny(router, body, callback)</td>
    <td style="padding:15px">Fib Table Deny</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/fib-table/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFibTableFailure(router, body, callback)</td>
    <td style="padding:15px">Failure Locking Fib Table</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/fib-table/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFibTableHit(router, body, callback)</td>
    <td style="padding:15px">FIB Lookup Hit</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/fib-table/hit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFibTableMiss(router, body, callback)</td>
    <td style="padding:15px">FIB Lookup Miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/fib-table/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFlowTableHit(router, body, callback)</td>
    <td style="padding:15px">Flow Lookup Hit</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/flow-table/hit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFlowTableInvalidated(router, body, callback)</td>
    <td style="padding:15px">Flow Lookup Invalidated Flow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/flow-table/invalidated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupFlowTableMiss(router, body, callback)</td>
    <td style="padding:15px">Flow Lookup Miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/flow-table/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupIcmpTableIpv4Allow(router, body, callback)</td>
    <td style="padding:15px">ICMP packets allowed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/icmp-table/ipv4/allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupIcmpTableIpv4Deny(router, body, callback)</td>
    <td style="padding:15px">ICMP packets dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/icmp-table/ipv4/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupIcmpTableIpv6Allow(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 packets allowed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/icmp-table/ipv6/allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupIcmpTableIpv6Deny(router, body, callback)</td>
    <td style="padding:15px">ICMPv6 packets dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/icmp-table/ipv6/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentEnableMetadataAllocationFailure(router, body, callback)</td>
    <td style="padding:15px">Sent Enable Metadata Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/sent-enable-metadata-allocation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentEnableMetadataCapacityFailure(router, body, callback)</td>
    <td style="padding:15px">Sent Enable Metadata Capacity Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/sent-enable-metadata-capacity-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSentEnableMetadataHmacFailure(router, body, callback)</td>
    <td style="padding:15px">Sent Enable Metadata Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/sent-enable-metadata-hmac-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupSentEnableMetadataSuccess(router, body, callback)</td>
    <td style="padding:15px">Sent Enable Metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/sent-enable-metadata-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupTenantTableFailure(router, body, callback)</td>
    <td style="padding:15px">Failure Locking Tenant Table</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/tenant-table/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupTenantTableHit(router, body, callback)</td>
    <td style="padding:15px">Tenant Table Hit</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/tenant-table/hit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingLookupTenantTableMiss(router, body, callback)</td>
    <td style="padding:15px">Tenant Table Miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/lookup/tenant-table/miss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingReceivedControlSuccess(router, body, callback)</td>
    <td style="padding:15px">Control Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/received/control-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingReceivedInjectedSuccess(router, body, callback)</td>
    <td style="padding:15px">Injected Packets Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/received/injected-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingReceivedInterfaceStandby(router, body, callback)</td>
    <td style="padding:15px">Packets Received on Standby Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/received/interface-standby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingReceivedInterfaceStandbyDrop(router, body, callback)</td>
    <td style="padding:15px">Packets Dropped on Standby Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/received/interface-standby-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingReceivedInterfaceSuccess(router, body, callback)</td>
    <td style="padding:15px">Packets Received from Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/received/interface-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingSentInterfaceFailure(router, body, callback)</td>
    <td style="padding:15px">Packet Transmit Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingSentInterfaceRetry(router, body, callback)</td>
    <td style="padding:15px">Packet Transmit Retries</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingSentInterfaceStandby(router, body, callback)</td>
    <td style="padding:15px">Packet Transmitted on Standby Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-standby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingSentInterfaceStandbyDrop(router, body, callback)</td>
    <td style="padding:15px">Packet Transmit Standby Drops</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-standby-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketProcessingSentInterfaceSuccess(router, body, callback)</td>
    <td style="padding:15px">Packets Transmitted</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterProcessingSentInterfaceUnconfiguredDrop(router, body, callback)</td>
    <td style="padding:15px">Packets Dropped on Unconfigured Interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/packet-processing/sent/interface-unconfigured-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedNotificationsDuration(router, body, callback)</td>
    <td style="padding:15px">Notification processing duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/notifications/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedNotificationsError(router, body, callback)</td>
    <td style="padding:15px">Total notifications failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/notifications/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedNotificationsTotal(router, body, callback)</td>
    <td style="padding:15px">Total notifications received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/notifications/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedResponsesDuration(router, body, callback)</td>
    <td style="padding:15px">Response processing duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/responses/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedResponsesError(router, body, callback)</td>
    <td style="padding:15px">Total responses failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/responses/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedResponsesTotal(router, body, callback)</td>
    <td style="padding:15px">Total responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/responses/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientReceivedUnknownTotal(router, body, callback)</td>
    <td style="padding:15px">Total responses received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/received/unknown/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientSentRequestsError(router, body, callback)</td>
    <td style="padding:15px">Total requests failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/sent/requests/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientSentRequestsTotal(router, body, callback)</td>
    <td style="padding:15px">Total requests sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/sent/requests/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTreeClientTransactionsServerLatency(router, body, callback)</td>
    <td style="padding:15px">Transaction server latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/client/transactions/server-latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReceivedRequestsAverage(router, body, callback)</td>
    <td style="padding:15px">Running average requests in the window</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/received/requests/average?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReceivedRequestsError(router, body, callback)</td>
    <td style="padding:15px">Total requests invalid</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/received/requests/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReceivedRequestsIgnored(router, body, callback)</td>
    <td style="padding:15px">Total requests ignored</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/received/requests/ignored?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReceivedRequestsTotal(router, body, callback)</td>
    <td style="padding:15px">Total requests received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/received/requests/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerSentResponsesError(router, body, callback)</td>
    <td style="padding:15px">Total responses failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/sent/responses/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerSentResponsesTotal(router, body, callback)</td>
    <td style="padding:15px">Total responses sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/sent/responses/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsConnectDuration(router, body, callback)</td>
    <td style="padding:15px">Connect transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/connect/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsConnectError(router, body, callback)</td>
    <td style="padding:15px">Total connect failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/connect/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsConnectProcessed(router, body, callback)</td>
    <td style="padding:15px">Total connect processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/connect/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsDisconnectDuration(router, body, callback)</td>
    <td style="padding:15px">Disconnect transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/disconnect/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsDisconnectError(router, body, callback)</td>
    <td style="padding:15px">Total disconnect failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/disconnect/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsDisconnectProcessed(router, body, callback)</td>
    <td style="padding:15px">Total disconnect processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/disconnect/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsListDuration(router, body, callback)</td>
    <td style="padding:15px">List transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/list/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsListError(router, body, callback)</td>
    <td style="padding:15px">Total lists failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/list/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsListProcessed(router, body, callback)</td>
    <td style="padding:15px">Total lists processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/list/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPingDuration(router, body, callback)</td>
    <td style="padding:15px">Ping transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/ping/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPingError(router, body, callback)</td>
    <td style="padding:15px">Total ping failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/ping/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPingProcessed(router, body, callback)</td>
    <td style="padding:15px">Total ping processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/ping/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPublishDuration(router, body, callback)</td>
    <td style="padding:15px">Publish transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/publish/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPublishError(router, body, callback)</td>
    <td style="padding:15px">Total publish failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/publish/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsPublishProcessed(router, body, callback)</td>
    <td style="padding:15px">Total publish processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/publish/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsReadDuration(router, body, callback)</td>
    <td style="padding:15px">Read transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/read/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsReadError(router, body, callback)</td>
    <td style="padding:15px">Total reads failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/read/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsReadProcessed(router, body, callback)</td>
    <td style="padding:15px">Total reads processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/read/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsRemoveDuration(router, body, callback)</td>
    <td style="padding:15px">Remove transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/remove/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsRemoveError(router, body, callback)</td>
    <td style="padding:15px">Total removals failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/remove/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsRemoveProcessed(router, body, callback)</td>
    <td style="padding:15px">Total removals processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/remove/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUnsubscribeDuration(router, body, callback)</td>
    <td style="padding:15px">Unsubscribe transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/unsubscribe/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUnsubscribeError(router, body, callback)</td>
    <td style="padding:15px">Total unsubscribes failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/unsubscribe/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUnsubscribeProcessed(router, body, callback)</td>
    <td style="padding:15px">Total unsubscribes processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/unsubscribe/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUpdateDuration(router, body, callback)</td>
    <td style="padding:15px">Update transaction duration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/update/duration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUpdateError(router, body, callback)</td>
    <td style="padding:15px">Total updates failed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/update/error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerTransactionsUpdateProcessed(router, body, callback)</td>
    <td style="padding:15px">Total updates processed</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/path-tree/server/transactions/update/processed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPerformanceMonitoringPeerPathJitter(router, body, callback)</td>
    <td style="padding:15px">Peer Path Jitter</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/performance-monitoring/peer-path/jitter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPerformanceMonitoringPeerPathLatency(router, body, callback)</td>
    <td style="padding:15px">Peer Path Latency</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/performance-monitoring/peer-path/latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPerformanceMonitoringPeerPathLoss(router, body, callback)</td>
    <td style="padding:15px">Peer Path Loss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/performance-monitoring/peer-path/loss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPerformanceMonitoringPeerPathMos(router, body, callback)</td>
    <td style="padding:15px">Peer Path MOS value</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/performance-monitoring/peer-path/mos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsProcessCpuUsage(router, body, callback)</td>
    <td style="padding:15px">Process CPU Usage</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/process/cpu/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsProcessMemoryRss(router, body, callback)</td>
    <td style="padding:15px">Process RSS Memory</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/process/memory/rss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsProcessMemoryVsz(router, body, callback)</td>
    <td style="padding:15px">Process VSZ Memory</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/process/memory/vsz?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerManagerRedisInactives(router, body, callback)</td>
    <td style="padding:15px">Redis Server Inactive Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redis-server-manager/redis-server-inactives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterManagerRedisServerNetworkFlaps(router, body, callback)</td>
    <td style="padding:15px">Redis Server Network Flap errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redis-server-manager/redis-server-network-flaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerManagerRedisSessionExpirations(router, body, callback)</td>
    <td style="padding:15px">Redis Session Expiration Errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redis-server-manager/redis-session-expirations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyDatabaseConnectionActiveConnections(router, body, callback)</td>
    <td style="padding:15px">Active connections to the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/active-connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyDatabaseConnectionClientDisconnect(router, body, callback)</td>
    <td style="padding:15px">Successfully disconnected from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/client-disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyDatabaseConnectionConnect(router, body, callback)</td>
    <td style="padding:15px">Successfully established connection</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyDatabaseConnectionErrorDisconnect(router, body, callback)</td>
    <td style="padding:15px">Disconnected from database due to internal error</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/error-disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyDatabaseConnectionErrors(router, body, callback)</td>
    <td style="padding:15px">Internal errors reported from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyDatabaseConnectionFailedConnect(router, body, callback)</td>
    <td style="padding:15px">Failed attempt at connecting to database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/database-connection/failed-connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPortInitializationMarkerQueryPending(router, body, callback)</td>
    <td style="padding:15px">Marker query read pending response from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/marker-query-pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPortInitializationMarkerRecordFound(router, body, callback)</td>
    <td style="padding:15px">Marker record already exists in database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/marker-record-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInitializationMarkerRecordNotFound(router, body, callback)</td>
    <td style="padding:15px">Marker record doesn't exist in database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/marker-record-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInitializationMarkerRecordReadError(router, body, callback)</td>
    <td style="padding:15px">Marker record read error from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/marker-record-read-error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPortInitializationMarkerRecordWrite(router, body, callback)</td>
    <td style="padding:15px">Marker record written to the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/marker-record-write?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyPortInitializationPendingQueries(router, body, callback)</td>
    <td style="padding:15px">Pending queries waiting for connection.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-initialization/pending-queries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterListRecordsRecordFound(router, body, callback)</td>
    <td style="padding:15px">List record entry found from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordNotFound(router, body, callback)</td>
    <td style="padding:15px">List record entry not found from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordParseError(router, body, callback)</td>
    <td style="padding:15px">Error parsing list record entry popped from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-parse-error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordPopErrors(router, body, callback)</td>
    <td style="padding:15px">Error popping list record entry from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-pop-errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordPopFailures(router, body, callback)</td>
    <td style="padding:15px">No records found in the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-pop-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordPopPending(router, body, callback)</td>
    <td style="padding:15px">List pop query pending response from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-pop-pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRecordsListRecordPopSuccess(router, body, callback)</td>
    <td style="padding:15px">List record entry successfully popped from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-pop-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterListRecordsRecordPush(router, body, callback)</td>
    <td style="padding:15px">List record entry pushed to the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/port-list-records/list-record-push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancySessionDelete(router, body, callback)</td>
    <td style="padding:15px">Session deleted from the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancySessionErrorsCacheTimeouts(router, body, callback)</td>
    <td style="padding:15px">Failure to reconstruct session due to packet cache timeouts</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/cache-timeouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancySessionErrorsDecodeFailures(router, body, callback)</td>
    <td style="padding:15px">internal decode failures while processing redundancy buffer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/decode-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsFibLookupError(router, body, callback)</td>
    <td style="padding:15px">Failed to recover session due to fib miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/fib-lookup-error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsInvalidBufferReceived(router, body, callback)</td>
    <td style="padding:15px">Invalid buffer received from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/invalid-buffer-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsInvalidKey(router, body, callback)</td>
    <td style="padding:15px">Invalid session-key received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/invalid-session-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterErrorsNewSessionCreationFailure(router, body, callback)</td>
    <td style="padding:15px">Failure in creating new regular session.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/new-session-creation-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsNotFound(router, body, callback)</td>
    <td style="padding:15px">Redundancy session expected but not found in session table</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/session-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsUpdateFailures(router, body, callback)</td>
    <td style="padding:15px">failures to update session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/session-update-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionErrorsSourceLookupError(router, body, callback)</td>
    <td style="padding:15px">Failed to recover session due to source lookup miss</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/source-lookup-error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancySessionErrorsUnsupportedFeature(router, body, callback)</td>
    <td style="padding:15px">Attempt to use an unsupported feature.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-errors/unsupported-feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionReadsCreateNew(router, body, callback)</td>
    <td style="padding:15px">New session being created due to database miss.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/create-new-session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionReadsNetUnreachableSent(router, body, callback)</td>
    <td style="padding:15px">ICMP Network Unreachable packet sent after query failure.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/net-unreachable-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancySessionReadsQueryPending(router, body, callback)</td>
    <td style="padding:15px">Read query pending response from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/query-pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReadsQueryResultNotFound(router, body, callback)</td>
    <td style="padding:15px">Result for read query not found in database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/query-result-not-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionReadsQueryResultSuccess(router, body, callback)</td>
    <td style="padding:15px">Result for read query successfully found in database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/query-result-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionReadsQueryResultTimeout(router, body, callback)</td>
    <td style="padding:15px">Result for read query timed out waiting for result from database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/query-result-timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionReadsTcpResetSent(router, body, callback)</td>
    <td style="padding:15px">TCP RST packet sent due to query failure.</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reads/tcp-reset-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterCreatedInterNodeServicePaths(router, body, callback)</td>
    <td style="padding:15px">Successfully reconstructed a inter-node service-path from redundancy buffer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reconstruction/created-inter-node-service-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterCreatedInterServicePaths(router, body, callback)</td>
    <td style="padding:15px">Successfully reconstructed a inter-router service-path from redundancy buffer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reconstruction/created-inter-router-service-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReconstructionCreatedLocalServicePaths(router, body, callback)</td>
    <td style="padding:15px">Successfully reconstructed a local service-path from redundancy buffer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reconstruction/created-local-service-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDivertedToExternalProtocolAgent(router, body, callback)</td>
    <td style="padding:15px">Successfully diverted packets to external-protocol-agent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-reconstruction/diverted-to-external-protocol-agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancySessionRefresh(router, body, callback)</td>
    <td style="padding:15px">Session Refresh to the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionWritesErrorsEncodeFailures(router, body, callback)</td>
    <td style="padding:15px">internal encode failures while processing redundancy buffer</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-writes/errors/encode-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWritesErrorsFromDatabase(router, body, callback)</td>
    <td style="padding:15px">errors received from database during write operations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-writes/errors/errors-from-database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionWritesWriteRequestSent(router, body, callback)</td>
    <td style="padding:15px">Session write requests to the database</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-writes/write-request-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionWritesWriteRequestSuccess(router, body, callback)</td>
    <td style="padding:15px">Session write requests to the database were successful</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/session-writes/write-request-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpAdvertisementsDropped(router, body, callback)</td>
    <td style="padding:15px">Advertisements dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/advertisements-dropped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpAdvertisementsReceived(router, body, callback)</td>
    <td style="padding:15px">Advertisements received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/advertisements-received?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpAdvertisementsSent(router, body, callback)</td>
    <td style="padding:15px">Advertisements sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/advertisements-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpBecomeBackup(router, body, callback)</td>
    <td style="padding:15px">Become Backup count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/become-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpBecomeMaster(router, body, callback)</td>
    <td style="padding:15px">Become Master count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/become-master?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyVrrpBecomeMasterFailure(router, body, callback)</td>
    <td style="padding:15px">Become Master failure count</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/become-master-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRedundancyVrrpReceivedInvalid(router, body, callback)</td>
    <td style="padding:15px">Invalid packet received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/received-invalid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyVrrpReceivedInvalidTtl(router, body, callback)</td>
    <td style="padding:15px">Invalid TTL packet received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/received-invalid-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyVrrpReceivedInvalidType(router, body, callback)</td>
    <td style="padding:15px">Invalid type packet received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/received-invalid-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRedundancyVrrpReceivedInvalidVersion(router, body, callback)</td>
    <td style="padding:15px">Invalid version packet received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/redundancy/vrrp/received-invalid-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRegisteredServicesLocal(router, body, callback)</td>
    <td style="padding:15px">Number of Local Registered Services</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/registered-services/local?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRegisteredServicesRemote(router, body, callback)</td>
    <td style="padding:15px">Number of Remote Registered Services</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/registered-services/remote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRegisteredServicesTotal(router, body, callback)</td>
    <td style="padding:15px">Number of Total Registered Services</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/registered-services/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingAgentAddRoute(router, body, callback)</td>
    <td style="padding:15px">Routes Added</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing-agent/add-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingAgentDeleteRoute(router, body, callback)</td>
    <td style="padding:15px">Routes Deleted</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing-agent/delete-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRoutingAgentFibInstallErrors(router, body, callback)</td>
    <td style="padding:15px">FIB installation errors</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing-agent/fib-install-errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingAgentFibMeter(router, body, callback)</td>
    <td style="padding:15px">Active FIB Routes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing-agent/fib-meter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingAgentInstallRoute(router, body, callback)</td>
    <td style="padding:15px">Route Installs</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing-agent/install-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingErrorsConnection(router, body, callback)</td>
    <td style="padding:15px">Route Manager Client Connection Error</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/errors/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRoutingRouteUpdatesReceivedAdd(router, body, callback)</td>
    <td style="padding:15px">Add Routes Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRouteUpdatesReceivedClearFib(router, body, callback)</td>
    <td style="padding:15px">Clear FIB Messages Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/clear-fib?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRoutingRouteUpdatesReceivedDelete(router, body, callback)</td>
    <td style="padding:15px">Delete Routes Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdatesReceivedEndOfFib(router, body, callback)</td>
    <td style="padding:15px">End of FIB Messages Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/end-of-fib?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRouteUpdatesReceivedIamActive(router, body, callback)</td>
    <td style="padding:15px">I am Active Messages Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/i-am-active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRouteUpdatesReceivedIamStandby(router, body, callback)</td>
    <td style="padding:15px">I am Standby Messages Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/i-am-standby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRoutingRouteUpdatesReceivedTotal(router, body, callback)</td>
    <td style="padding:15px">Total Route Updates Received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/received/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRoutingRouteUpdatesSentTotal(router, body, callback)</td>
    <td style="padding:15px">Total Route Updates Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/route-updates/sent/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsRoutingRoutes(router, body, callback)</td>
    <td style="padding:15px">Active Routes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/routing/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaDhcpUnknownDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped unknown packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/unknown-dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientAckDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped ack packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/ack/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientAckProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed ack packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/ack/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientAckSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent ack packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/ack/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDeclineDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped decline packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/decline/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDeclineProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed decline packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/decline/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDeclineSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent decline packets sent fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/decline/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDiscoverDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped discover packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/discover/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDiscoverProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed discover packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/discover/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientDiscoverSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent discover packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/discover/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpV4ClientDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientNackDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped nack packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/nack/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientNackProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed nack packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/nack/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientNackSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent nack packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/nack/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientOfferDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped offer packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/offer/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientOfferProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed offer packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/offer/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientOfferSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent offer packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/offer/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpV4ClientProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientReleaseDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped release packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/release/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientReleaseProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed release packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/release/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientReleaseSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent release packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/release/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientRequestDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped request packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/request/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientRequestProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed request packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/request/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientRequestSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent request packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/request/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDhcpV4ClientSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientUnknownDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped unknown DHCP packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/unknown/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientUnknownProcessedPackets(router, body, callback)</td>
    <td style="padding:15px">Processed unknown DHCP packets from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/unknown/processed-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4ClientUnknownSentPackets(router, body, callback)</td>
    <td style="padding:15px">Sent unknown packets to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/client/unknown/sent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4RelayReceivedInvalidPackets(router, body, callback)</td>
    <td style="padding:15px">Packets received that were generally invalid</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/received/invalid-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRelayReceivedInvalidRequestPackets(router, body, callback)</td>
    <td style="padding:15px">Invalid request packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/received/invalid-request-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRelayReceivedInvalidResponsePackets(router, body, callback)</td>
    <td style="padding:15px">Invalid response packets</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/received/invalid-response-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4RelayReceivedRequestPackets(router, body, callback)</td>
    <td style="padding:15px">Request packets received from a DHCP client</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/received/request-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4RelayReceivedResponsePackets(router, body, callback)</td>
    <td style="padding:15px">Response packets received from a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/received/response-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4RelayTransmittedRequestPackets(router, body, callback)</td>
    <td style="padding:15px">Request packets sent to a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/transmitted/request-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterV4RelayTransmittedResponsePackets(router, body, callback)</td>
    <td style="padding:15px">Packets sent to a DHCP client</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v4/relay/transmitted/response-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientRequestReceivedFromApplication(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 client request packets received from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/client-request/received-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterRequestSentToFastLane(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 client request packets sent to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/client-request/sent-to-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAdvertisementDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Router advertisement packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/router-advertisement/dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAdvertisementReceivedFromFastLane(router, body, callback)</td>
    <td style="padding:15px">Router advertisement packets received from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/router-advertisement/received-from-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAdvertisementSentToApplication(router, body, callback)</td>
    <td style="padding:15px">Router advertisement packets sent to application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/router-advertisement/sent-to-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSolicitationReceivedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Router solicitation packets received from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/router-solicitation/received-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSolicitationSentToFastLane(router, body, callback)</td>
    <td style="padding:15px">Router solicitation packets sent to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/router-solicitation/sent-to-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReplyDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 server reply packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/server-reply/dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReplyReceivedFromFastLane(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 server reply packets received from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/server-reply/received-from-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReplySentToApplication(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 server reply packets sent to application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/server-reply/sent-to-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientUnknownDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Unknown packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/client/unknown-dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterClientRequestDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 client request packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/client-request/dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAdvertisementReceivedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Router advertisement packets received from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/router-advertisement/received-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAdvertisementSentToFastLane(router, body, callback)</td>
    <td style="padding:15px">Router advertisement packets sent to fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/router-advertisement/sent-to-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSolicitationDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Router solicitation packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/router-solicitation/dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSolicitationReceivedFromFastLane(router, body, callback)</td>
    <td style="padding:15px">Router solicitation packets received from fast lane</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/router-solicitation/received-from-fast-lane?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSolicitationSentToApplication(router, body, callback)</td>
    <td style="padding:15px">Router solicitation packets sent to application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/router-solicitation/sent-to-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerReplyDroppedFromApplication2(router, body, callback)</td>
    <td style="padding:15px">DHCPv6 server reply packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/server-reply/dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServerUnknownDroppedFromApplication(router, body, callback)</td>
    <td style="padding:15px">Unknown packets dropped from application</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/dhcp/v6/router-advertisement-server/unknown-dropped-from-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedAdaptiveEncryptionModifyPackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets on flows requiring modification for adaptive encryption</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/adaptive-encryption-modify-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaReceivedCollisionModifyPackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets on flows requiring modification by the Service Area</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/collision-modify-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDroppedIcmpRequestPacketBlackhole(router, body, callback)</td>
    <td style="padding:15px">Dropped ICMP Request packets due to ICMP Blackhole</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/dropped-icmp-request-packet-blackhole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterIcmpRequestPacketNoEgress(router, body, callback)</td>
    <td style="padding:15px">Dropped ICMP Request packets due to missing Egress interface</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/dropped-icmp-request-packet-no-egress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceAreaReceivedDroppedPackets(router, body, callback)</td>
    <td style="padding:15px">Packets Dropped by the Service Area</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/dropped-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDroppedTapMultiplexRecoveryPackets(router, body, callback)</td>
    <td style="padding:15px">Dropped packets for multiplex session recovery</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/dropped-tap-multiplex-recovery-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedDuplicateReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Duplicate reverse metadata packets received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/duplicate-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedEmptyReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Empty reverse metadata packets received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/empty-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceAreaReceivedExistingFlows(router, body, callback)</td>
    <td style="padding:15px">Packets Received For Existing Flows</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/existing-flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceAreaReceivedExistingSession(router, body, callback)</td>
    <td style="padding:15px">Packets matching an existing session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/existing-session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterExistingSessionReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packets with key that matches another existing session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/existing-session-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedExternalProtocolAgentPackets(router, body, callback)</td>
    <td style="padding:15px">external protocol agent packets to be redirected</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/external-protocol-agent-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterServiceAreaReceivedFabricPackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets From Within The Fabric</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/fabric-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFibBadSharedMemEntry(router, body, callback)</td>
    <td style="padding:15px">Bad Shared Memory Entry for FIB lookup</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/fib-bad-shared-mem-entry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFlowBadSharedMemEntry(router, body, callback)</td>
    <td style="padding:15px">Bad Shared Memory Entry for flow lookup</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/flow-bad-shared-mem-entry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterFlowExpiredReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packets received with no matching flow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/flow-expired-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaReceivedFlowMovePackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets on flows requiring modification to use a better path</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/flow-move-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedInvalidReverseFlowModify(router, body, callback)</td>
    <td style="padding:15px">Reverse flow packet causing a flow modify received in service area</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/invalid-reverse-flow-modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedInvalidReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Invalid reverse metadata packets received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/invalid-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedMidFlowModifyPackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets on flows requiring modification by the Service Area</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/mid-flow-modify-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaReceivedNoFibEntry(router, body, callback)</td>
    <td style="padding:15px">Failed to match FIB entry</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/no-fib-entry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaReceivedNonFabricPackets(router, body, callback)</td>
    <td style="padding:15px">Received Packets From Outside The Fabric</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/non-fabric-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPacketsWithStaleFlowHit(router, body, callback)</td>
    <td style="padding:15px">Packets with stale flow hit that do not have a session associated with it</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/packets-with-stale-flow-hit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedReverseMetadataProcessFailure(router, body, callback)</td>
    <td style="padding:15px">Failed to process reverse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/reverse-metadata-process-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedSessionCollisionChangeDirection(router, body, callback)</td>
    <td style="padding:15px">Packets matching an existing session that changed session direction</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/session-collision-change-direction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedSessionCollisionIgnoreDrop(router, body, callback)</td>
    <td style="padding:15px">Packets matching an existing session with a mismatched service</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/session-collision-ignore-drop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaReceivedSessionCollisionIgnored(router, body, callback)</td>
    <td style="padding:15px">Packets matching an existing session that do not change the session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/session-collision-ignored?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionIgnoredReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packets that are ignored when received from another router</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/session-ignored-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionUpdatedReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Reverse metadata packets that successfully updated the session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/session-updated-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterLookupBadSharedMemEntry(router, body, callback)</td>
    <td style="padding:15px">Bad Shared Memory Entry for source lookup</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/source-lookup-bad-shared-mem-entry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateFlowWithPeerFailure(router, body, callback)</td>
    <td style="padding:15px">Failed to update flow with destination peer received in reverse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/update-flow-with-peer-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateFlowWithPeerSuccess(router, body, callback)</td>
    <td style="padding:15px">Successfully updated flow with destination peer received in reverse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/update-flow-with-peer-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateSessionWithPeerFailure(router, body, callback)</td>
    <td style="padding:15px">Failed to update session with destination peer received in reverse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/update-session-with-peer-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterUpdateSessionWithPeerSuccess(router, body, callback)</td>
    <td style="padding:15px">Successfully updated session with destination peer received in reverse metadata</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/update-session-with-peer-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedValidReverseMetadataPackets(router, body, callback)</td>
    <td style="padding:15px">Valid reverse metadata packets received</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/received/valid-reverse-metadata-packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaSentArpResolveFailure(router, body, callback)</td>
    <td style="padding:15px">Packets Dropped due to unsuccessful ARP resolution</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/sent/arp-resolve-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaSentArpResolvePending(router, body, callback)</td>
    <td style="padding:15px">Packets Queued Pending Valid ARP</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/sent/arp-resolve-pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAreaSentArpResolveSuccess(router, body, callback)</td>
    <td style="padding:15px">Packets Sent after successful ARP resolution</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/sent/arp-resolve-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsServiceAreaSentSuccess(router, body, callback)</td>
    <td style="padding:15px">Packets Sent Successfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/sent/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterResetForAdaptiveEncryptionFailure(router, body, callback)</td>
    <td style="padding:15px">Tcp reset packets sent due to adaptive encryption setup failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/sent/tcp-reset-for-adaptive-encryption-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInstallErrorsDuplicateReverseFlow(router, body, callback)</td>
    <td style="padding:15px">Flows not installed due to duplicate reverse flow</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/duplicate-reverse-flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionInstallErrorsFibLookup(router, body, callback)</td>
    <td style="padding:15px">FIB Lookup Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/fib-lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInstallErrorsGatewayLookupFailure(router, body, callback)</td>
    <td style="padding:15px">Gateway Lookup failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/gateway-lookup-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionInstallErrorsInvalidPacket(router, body, callback)</td>
    <td style="padding:15px">Invalid Packet Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/invalid-packet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInstallErrorsServicePathsUnavailable(router, body, callback)</td>
    <td style="padding:15px">Session not installed due to service paths not being available</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/service-paths-unavailable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInstallErrorsUrpfCheckFailure(router, body, callback)</td>
    <td style="padding:15px">uRPF check failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/urpf-check-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionInstallErrorsWaypointAllocation(router, body, callback)</td>
    <td style="padding:15px">Waypoint Allocation Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/service-area/session-install-errors/waypoint-allocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionActive(router, body, callback)</td>
    <td style="padding:15px">Active Session</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionAdd(router, body, callback)</td>
    <td style="padding:15px">Added Sessions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionAddedExisting(router, body, callback)</td>
    <td style="padding:15px">Duplicate Adds</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/added-existing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionDuplicateId(router, body, callback)</td>
    <td style="padding:15px">Duplicate Session Id</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/duplicate-session-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionFlowActive(router, body, callback)</td>
    <td style="padding:15px">Active Flows</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/flow/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionFlowAddFailure(router, body, callback)</td>
    <td style="padding:15px">Flow Add Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/flow/add/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionFlowRemoveFailure(router, body, callback)</td>
    <td style="padding:15px">Flow Removal Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/flow/remove/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSessionMaxRemoveTimeoutExceeded(router, body, callback)</td>
    <td style="padding:15px">Maximum Remove Time Exceeded</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/max-remove-timeout-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSessionRemove(router, body, callback)</td>
    <td style="padding:15px">Removed Sessions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/session/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSourceNatActiveTables(router, body, callback)</td>
    <td style="padding:15px">Active source-nat tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/active-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNatAllocatePortsForDb(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were allocated to db</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/allocate-ports-for-db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNatGiidTransitionToActive(router, body, callback)</td>
    <td style="padding:15px">Number of notifications for interface transitioning to active</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/giid-transition-to-active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNatGiidTransitionToInactive(router, body, callback)</td>
    <td style="padding:15px">Number of notifications for interface transitioning to inactive</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/giid-transition-to-inactive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSourceNatInactiveTables(router, body, callback)</td>
    <td style="padding:15px">Inactive source-nat tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/inactive-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterNatReinitializePortsInDb(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were reinitialized after recovery</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/reinitialize-ports-in-db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReleasePortsToDbFailure(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were released back to database unsuccessfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/release-ports-to-db-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReleasePortsToDbSuccess(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were released back to database successfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/release-ports-to-db-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSourceNatReservedPorts(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were reserved during recovery</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-nat/reserved-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSourceTenantMeter(router, body, callback)</td>
    <td style="padding:15px">Source Tenant table meter</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/source-tenant/meter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscClientsConnects(router, body, callback)</td>
    <td style="padding:15px">Count of Client Connect Events</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/clients/connects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscClientsDisconnects(router, body, callback)</td>
    <td style="padding:15px">Count of Client Disconnect Events</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/clients/disconnects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscOperationsCompressions(router, body, callback)</td>
    <td style="padding:15px">Number of Compression Operations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/operations/compressions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscOperationsDecompressions(router, body, callback)</td>
    <td style="padding:15px">Number of Decompression Operations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/operations/decompressions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedByServiceDeregistrations(router, body, callback)</td>
    <td style="padding:15px">Number of Deregistrations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/deregistrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedByServicePublishes(router, body, callback)</td>
    <td style="padding:15px">Number of Publishes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/publishes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedByServiceRegistrationUpdates(router, body, callback)</td>
    <td style="padding:15px">Number of Registration Updates</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/registration-updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedByServiceRegistrations(router, body, callback)</td>
    <td style="padding:15px">Number of Registrations</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/registrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedByServiceRequests(router, body, callback)</td>
    <td style="padding:15px">Number of Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedByServiceResponses(router, body, callback)</td>
    <td style="padding:15px">Number of Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/by-service/responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsBadRequestResponses(router, body, callback)</td>
    <td style="padding:15px">Bad Request Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/bad-request-responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedErrorsInvalidMessage(router, body, callback)</td>
    <td style="padding:15px">Invalid Messages</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/invalid-message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscReceivedErrorsInvalidRequest(router, body, callback)</td>
    <td style="padding:15px">Invalid SSC Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/invalid-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsNoClientTransaction(router, body, callback)</td>
    <td style="padding:15px">SSC No Client Transaction</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/no-client-transaction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsNoEndpointsFound(router, body, callback)</td>
    <td style="padding:15px">No Endpoints Found</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/no-endpoints-found?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsResponseNotReady(router, body, callback)</td>
    <td style="padding:15px">Responses Not Ready</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/response-not-ready?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsServerErrorResponses(router, body, callback)</td>
    <td style="padding:15px">Server Error Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/server-error-responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsUnknownMessageType(router, body, callback)</td>
    <td style="padding:15px">Unknown Message type</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/unknown-message-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReceivedErrorsUnknownSscRequest(router, body, callback)</td>
    <td style="padding:15px">Unknown SSC Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/received/errors/unknown-ssc-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscSessionsActive(router, body, callback)</td>
    <td style="padding:15px">Number of Active Sessions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/sessions/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscTransactionsClient(router, body, callback)</td>
    <td style="padding:15px">Number of Client Transactions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transactions/client-transactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscTransactionsErrorsInsertionFailures(router, body, callback)</td>
    <td style="padding:15px">Insertion Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transactions/errors/insertion-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscTransactionsServer(router, body, callback)</td>
    <td style="padding:15px">Number of Server Transactions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transactions/server-transactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscTransmittedByServicePublishes(router, body, callback)</td>
    <td style="padding:15px">Number of Publishes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/by-service/publishes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscTransmittedByServiceRequests(router, body, callback)</td>
    <td style="padding:15px">Number of Requests</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/by-service/requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscTransmittedCompletedResponses(router, body, callback)</td>
    <td style="padding:15px">Completed Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/completed-responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscTransmittedErrorsWriteFailures(router, body, callback)</td>
    <td style="padding:15px">Write Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/errors/write-failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscTransmittedLocalResponses(router, body, callback)</td>
    <td style="padding:15px">Local Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/local-responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterSscTransmittedPublishOnRegistrations(router, body, callback)</td>
    <td style="padding:15px">Publishes on registration</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/publish-on-registrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsSscTransmittedRetransmittedResponses(router, body, callback)</td>
    <td style="padding:15px">Retransmitted Responses</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/ssc/transmitted/retransmitted-responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDevicePortQueueDepthAvg(router, body, callback)</td>
    <td style="padding:15px">Device Port Traffic Class Average Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/queue-depth/avg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDevicePortQueueDepthMax(router, body, callback)</td>
    <td style="padding:15px">Device Port Traffic Class Maximum Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/queue-depth/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDevicePortQueueDepthMin(router, body, callback)</td>
    <td style="padding:15px">Device Port Traffic Class Minimum Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/queue-depth/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterDevicePortQueueDepthTotal(router, body, callback)</td>
    <td style="padding:15px">Device Port Traffic Class Total Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/queue-depth/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngDevicePortScheduleFailure(router, body, callback)</td>
    <td style="padding:15px">Device Port Scheduler Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/schedule-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngDevicePortScheduleSuccess(router, body, callback)</td>
    <td style="padding:15px">Device Port Scheduler Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/device-port/schedule-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInternalApplicationQueueDepthAvg(router, body, callback)</td>
    <td style="padding:15px">Application Traffic Class Average Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/queue-depth/avg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInternalApplicationQueueDepthMax(router, body, callback)</td>
    <td style="padding:15px">Application Traffic Class Maximum Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/queue-depth/max?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInternalApplicationQueueDepthMin(router, body, callback)</td>
    <td style="padding:15px">Application Traffic Class Minimum Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/queue-depth/min?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterInternalApplicationQueueDepthTotal(router, body, callback)</td>
    <td style="padding:15px">Application Traffic Class Total Queue Depth</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/queue-depth/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngInternalApplicationScheduleFailure(router, body, callback)</td>
    <td style="padding:15px">Internal Application Scheduler Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/schedule-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngInternalApplicationScheduleSuccess(router, body, callback)</td>
    <td style="padding:15px">Internal Application Scheduler Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/schedule-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngInternalApplicationSentRetry(router, body, callback)</td>
    <td style="padding:15px">Internal Application Sent Failure</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/sent-retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterEngInternalApplicationSentSuccess(router, body, callback)</td>
    <td style="padding:15px">Internal Application Sent Success</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-eng/internal-application/sent-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsTrafficExportEnqueueFailure(router, body, callback)</td>
    <td style="padding:15px">Event enqueue failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-export/enqueue-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterTrafficExportRateLimitExceeded(router, body, callback)</td>
    <td style="padding:15px">Events dropped</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-export/rate-limit-exceeded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsTrafficExportTransmitBatches(router, body, callback)</td>
    <td style="padding:15px">Batches of events</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-export/transmit/batches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsTrafficExportTransmitFailure(router, body, callback)</td>
    <td style="padding:15px">Event export failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-export/transmit/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsTrafficExportTransmitSuccess(router, body, callback)</td>
    <td style="padding:15px">Events exported successfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/traffic-export/transmit/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWaypointActiveInterTables(router, body, callback)</td>
    <td style="padding:15px">Active inter-router waypoint tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/active-inter-router-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPortsForDefaultRangeTables(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were allocated to db for default range tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/allocate-ports-for-default-range-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterPortsForRangeBasedTables(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were allocated to db for range based tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/allocate-ports-for-range-based-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWaypointGiidTransitionToActive(router, body, callback)</td>
    <td style="padding:15px">Number of notifications for interface transitioning to active</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/giid-transition-to-active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWaypointGiidTransitionToInactive(router, body, callback)</td>
    <td style="padding:15px">Number of notifications for interface transitioning to inactive</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/giid-transition-to-inactive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWaypointInactiveInterTables(router, body, callback)</td>
    <td style="padding:15px">Inactive inter-router waypoint tables</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/inactive-inter-router-tables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWaypointReinitializePortsInDb(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were reinitialized after recovery</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/reinitialize-ports-in-db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReleasePortsToDbFailure2(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were released back to database unsuccessfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/release-ports-to-db-failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterReleasePortsToDbSuccess2(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were released back to database successfully</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/release-ports-to-db-success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterStatsWaypointReservedPorts(router, body, callback)</td>
    <td style="padding:15px">Number of times ports were reserved during recovery</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/waypoint/reserved-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAesContextCreateFailure(router, body, callback)</td>
    <td style="padding:15px">AES Context Creation Failures</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/aes/context/create/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAesContextCreateSuccess(router, body, callback)</td>
    <td style="padding:15px">AES Context Creation Successes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/aes/context/create/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAesContextDestroyFailure(router, body, callback)</td>
    <td style="padding:15px">Failure to Destroy AES Context</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/aes/context/destroy/failure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterAesContextDestroySuccess(router, body, callback)</td>
    <td style="padding:15px">AES Contexts Destruction Successes</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/aes/context/destroy/success?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWorkerCoreCachingLoopException(router, body, callback)</td>
    <td style="padding:15px">Worker Core Caching Process Loop Exceptions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/caching-loop-exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWorkerCoreControlMessagesSent(router, body, callback)</td>
    <td style="padding:15px">Control Messages Sent</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/control-messages-sent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWorkerCorePacketProcessingUtilization(router, body, callback)</td>
    <td style="padding:15px">Packet Processing Core Utilization</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/packet-processing-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRouterWorkerCoreProcessLoopException(router, body, callback)</td>
    <td style="padding:15px">Worker Core Process Loop Exceptions</td>
    <td style="padding:15px">{base_path}/{version}/router/{pathv1}/stats/worker-core/process-loop-exception?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getService(startTime, endTime, router, callback)</td>
    <td style="padding:15px">Gets a list of all of the configured services.</td>
    <td style="padding:15px">{base_path}/{version}/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getService2(startTime, endTime, router, service, callback)</td>
    <td style="padding:15px">Gets a specific configured service.</td>
    <td style="padding:15px">{base_path}/{version}/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceClass(router, node, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets the corresponding service classes.</td>
    <td style="padding:15px">{base_path}/{version}/serviceClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(router, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets a list of all the configured tenants.</td>
    <td style="padding:15px">{base_path}/{version}/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant2(router, tenant, startTime, endTime, callback)</td>
    <td style="padding:15px">Gets a single configured tenant.</td>
    <td style="padding:15px">{base_path}/{version}/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
