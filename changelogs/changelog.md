
## 0.4.2 [06-23-2022]

* Update to CALLS.md to test header

See merge request itentialopensource/adapters/cloud/adapter-128technology!8

---

## 0.4.1 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/cloud/adapter-128technology!7

---

## 0.4.0 [05-18-2022] & 0.3.0 [01-19-2022]

- Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
- Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
- Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
- Add scripts for easier authentication, removing hooks, etc
- Script changes (install script as well as database changes in other scripts)
- Double # of path vars on generic call
- Update versions of foundation (e.g. adapter-utils)
- Update npm publish so it supports https
- Update dependencies
- Add preinstall for minimist
- Fix new lint issues that came from eslint dependency change
- Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
- Add the adapter type in the package.json
- Add AdapterInfo.js script
- Add json-query dependency
- Add the propertiesDecorators.json for product encryption
- Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
- Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
- Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
- Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
- Add AdapterInfo.json
- Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-128technology!5

---

## 0.2.4 [02-25-2021]

- migration to bring up to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-128technology!4

---

## 0.2.3 [07-06-2020] & 0.2.2 [07-01-2020]

- Migration of the adapter to the latest adapter foundation - specific details are in ADAPT-168

See merge request itentialopensource/adapters/cloud/adapter-128technology!3

---

## 0.2.1 [06-02-2020]

- Only change the sampleProperties so no code changes

See merge request itentialopensource/adapters/cloud/adapter-128technology!2

---

## 0.2.0 [04-07-2020]

- Fix the method names that were generated automatically as they are bad as a result of not having operation ids.

See merge request itentialopensource/adapters/cloud/adapter-128technology!1

---
