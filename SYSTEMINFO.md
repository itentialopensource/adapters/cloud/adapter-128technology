# 128 Technolgy

Vendor: Juniper Networks
Homepage: https://www.juniper.net/

Product: 128 Technolgy
Product Page: https://www.juniper.net/documentation/product/us/en/session-smart-router/

## Introduction
We classify 128 Technolgy into the Cloud domain as 128 Technolgy, known for its Session Smart Router, leverages software-defined networking to provide advanced routing solutions optimized for cloud environments.

"Provides Session Smart Routers with a service-centric control plane and a session-aware data plane" 

The 128 Technolgy adapter can be integrated to the Itential Device Broker which will allow your Devices to be managed within the Itential Configuration Manager Application. 

## Why Integrate
The 128 Technolgy adapter from Itential is used to integrate the Itential Automation Platform (IAP) with 128 Technolgy. With this adapter you have the ability to perform operations such as:

- Configuring Devices
- Create Service Resource
- Create Transport Resource
- Create Tenant Resource
- Create Access Policy
- Modify Service Route
- Validate, Revert, or Commit Candidate Configuration
- Get Device Configuration
- Upgrade SSR Networking Platform

## Additional Product Documentation
The [API documents for 128 Technolgy](https://www.juniper.net/documentation/us/en/software/session-smart-router/docs/api_rest_4.2.0/)