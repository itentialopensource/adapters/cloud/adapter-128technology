# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the 128technology System. The API that was used to build the adapter for 128technology is usually available in the report directory of this adapter. The adapter utilizes the 128technology API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The 128 Technolgy adapter from Itential is used to integrate the Itential Automation Platform (IAP) with 128 Technolgy. With this adapter you have the ability to perform operations such as:

- Configuring Devices
- Create Service Resource
- Create Transport Resource
- Create Tenant Resource
- Create Access Policy
- Modify Service Route
- Validate, Revert, or Commit Candidate Configuration
- Get Device Configuration
- Upgrade SSR Networking Platform

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
